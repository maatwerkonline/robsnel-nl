<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<div class="additional-navbar">
	<div class="additional-navbar--container">
		<a class="additional-navbar--link" href="tel:<?php echo get_theme_mod( 'phone_number_block'); ?>"> <i class="rs rs-phone"></i> <?php echo get_theme_mod( 'phone_number_block'); ?> </a>
		
	</div>
</div>

<div class="header-border">
	<header id="header">
		<nav class="navbar navbar-light" role="navigation">
			<div class="container">

				<?php wp_nav_menu( array( 'menu' => 'left', 'theme_location' => 'left', 'container_id' => 'left-menu', 'container_class' => 'd-none d-lg-block mr-auto', 'menu_class' => 'nav navbar-nav', 'fallback_cb' => false, 'walker' => new default_walker_nav_menu()) ); ?>

				<div class="navbar-brand">
					
					<?php
					
					if ( function_exists( 'the_custom_logo' )  && get_theme_mod( 'custom_logo' ) != '' ) : // If we have Custom Logo uploaded in the Customizer 
						
						the_custom_logo();
					
					else:  // If we have NO Custom Logo uploaded in the Customer ?>

						<a class="custom-logo-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">

							<p class="site-title mb-0 h6"><?php bloginfo( 'name' ); ?></p>

							<?php $description = get_bloginfo( 'description', 'display' );
							if ( $description || is_customize_preview() ) : // If we have a Site description ?>
								<p class="site-description mb-0"><small><?php echo $description; ?></small></p>
							<?php endif; ?>

						</a>

					<?php endif; ?>

				</div>
				
				<?php wp_nav_menu( array( 'menu' => 'right', 'theme_location' => 'right', 'container_id' => 'right-menu', 'container_class' => 'd-none d-lg-block ml-auto', 'menu_class' => 'nav navbar-nav', 'fallback_cb' => false, 'walker' => new default_walker_nav_menu()) ); ?>

				<div id="menu-toggle" class="d-block d-lg-none <?php if(is_page('offline')){ ?> inactive <?php }; ?>">
					<div class="hamburger">
						<div class="hamburger-box">
							<?php if(is_page('offline')){ ?>
								<i class="lnr-undo2"></i>
							<?php } else { ?>
								<div class="hamburger-inner"></div>
							<?php }; ?>
						</div>
					</div>
					<span class="mobile-menu-label"><?php if(is_page('offline')){ _e('Reload', 'maatwerkonline'); } else { _e('Menu', 'maatwerkonline'); }; ?></span>
				</div>

			</div>
		</nav>
		
	</header>
</div>