<?php
/**
 * Block Name: News query
 * This is the template that displays the block news-query.
 *
 * @author Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>"> <!-- News query block -->
	<?php
	$title = get_field('title');
	$link = get_field('button_link');
	?>
	<div class="news-query--background">
		<div class="content-holder">
			<div class="wp-bootstrap-blocks-container container">
				<div class="wp-bootstrap-blocks-row row">
					<div class="col-12 text-center">
						<h1 class="text-blue pb-3"><?php echo $title; ?></h1>
					</div>
					<?php
						// The Query
						$args = array (
							'post_type' => 'post',
							'post_per_page' => 3
						);
						$the_query = new WP_Query( $args );
						
						// The Loop
						if ( $the_query->have_posts() ) {
							while ( $the_query->have_posts() ) {
								$the_query->the_post();
								?>
								<div class="col-12 col-md-4">
									<a href="<?php echo get_permalink(); ?>">
										<div class="news-query--card">
											<img src="<?php echo get_the_post_thumbnail_url(); ?>">
											<h3 class="pt-3 mb-1"><?php echo get_the_title(); ?></h3>
											<span><h6><?php _e('Read more', 'maatwerkonline'); ?></h6></span>
										</div>
									</a>
								</div>
								<?php
							}
						};
						/* Restore original Post Data */
						wp_reset_postdata(); 
					?>
					<div class="col-12 d-flex">
						<li class="wp-block-button is-style-outline m-auto pt-5">
							<a class="wp-block-button__link" href="<?php echo $link; ?>"><?php _e('More news', 'maatwerkonline'); ?></a>
						</li>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>