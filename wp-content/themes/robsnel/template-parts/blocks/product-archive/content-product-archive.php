<?php
/**
 * Block Name: Product archive
 * This is the template that displays the block product-archive.
 *
 * @author Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>"> <!-- Product archive block -->
	<?php
	?>

	<div class="wp-bootstrap-blocks-container container">
		<div class="wp-bootstrap-blocks-row row">
			<div class="col-12 text-center">
				Categorieen
			</div>
			<div class="col-4">
				Filter
			</div>
			<div class="col-8">
				<div class="wp-bootstrap-blocks-container container">
					<div class="wp-bootstrap-blocks-row row">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>