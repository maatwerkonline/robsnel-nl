<?php
/**
 * Block Name: Services Block
 * This is the template that displays the block services-block.
 *
 * @author Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}
?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?> services"> <!-- You may change this element to another element (for example blockquote for quotes) -->
	<?php if( have_rows('services') ): $i=0; ?>
		<?php  while( have_rows('services') ) : the_row(); $i++; ?>
			<?php if ($i % 2 == 0) : ?>
				<div class="service service--odd">
					<div class="content-holder">
						<div class="row">
							<div class="col-12 col-xl-5 my-auto p-4 p-xl-0 order-3 order-xl-1">
								<h2 class="text-blue">
									<?php echo get_sub_field('title'); ?>
								</h2>
								<div class="text-blue">
									<?php echo get_sub_field('description'); ?>
								</div>
								<?php if(!empty(get_sub_field('first_button_name'))) : ?>
									<li class="wp-block-button is-style-fill mt-4">
										<a class="wp-block-button__link mr-3 mb-3 mb-xl-0" href="<?php echo get_sub_field('first_button_url'); ?>"><?php echo get_sub_field('first_button_name'); ?></a>
										<?php if(!empty(get_sub_field('second_button_name'))) : ?>
											<a class="wp-block-button__link" href="<?php echo get_sub_field('second_button_url'); ?>"><?php echo get_sub_field('second_button_name'); ?></a>
										<?php endif; ?>
									</li>
								<?php endif; ?>
							</div>
							<div class="col-12 col-xl-1">
							</div>
							<div class="col-12 col-xl-6 order-1 order-xl-3">
								<?php 
									$featured_image_id = get_sub_field('featured_image');
									$featured_image = wp_get_attachment_image_src( $featured_image_id, 'large');
								?>
								<div class="service__image_holder">
									<img class="service__image_holder__image" src="<?php echo $featured_image[0]; ?>">
									<img class="service__image_holder__overlay" src="<?php echo get_site_url(); ?>/wp-content/uploads/2022/01/blue-fade.png">
								</div>
							</div>		
						</div>
					</div>	
				</div>
			<?php else: ?>
				<div class="service service--even">
					<div class="content-holder">
						<div class="row">
							<div class="col-12 col-xl-6">
								<?php 
									$featured_image_id = get_sub_field('featured_image');
									$featured_image = wp_get_attachment_image_src( $featured_image_id, 'large');
								?>
								<div class="service__image_holder mb-4 mb-xl-0">
									<img class="service__image_holder__image" src="<?php echo $featured_image[0]; ?>">
									<img class="service__image_holder__overlay" src="<?php echo get_site_url(); ?>/wp-content/uploads/2022/01/blue-fade.png">
								</div>
							</div>	
							<div class="col-12 col-xl-5 my-auto p-4 p-xl-0">
								<h2 class="text-blue">
									<?php echo get_sub_field('title'); ?>
								</h2>
								<div class="text-blue">
									<?php echo get_sub_field('description'); ?>
								</div>	
								<?php if(!empty(get_sub_field('first_button_name'))) : ?>
									<li class="wp-block-button is-style-fill mt-4">
										<a class="wp-block-button__link mr-3 mb-3 mb-xl-0" href="<?php echo get_sub_field('first_button_url'); ?>"><?php echo get_sub_field('first_button_name'); ?></a>
										<?php if(!empty(get_sub_field('second_button_name'))) : ?>
											<a class="wp-block-button__link" href="<?php echo get_sub_field('second_button_url'); ?>"><?php echo get_sub_field('second_button_name'); ?></a>
										<?php endif; ?>
									</li>
								<?php endif; ?>
							</div>
							<div class="col-12 col-xl-1 my-auto">	
							</div>
						</div>
					</div>		
				</div>		
			<?php endif; ?>
		<?php endwhile; ?>
	<?php endif; ?>
</div>