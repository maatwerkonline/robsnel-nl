<?php
/**
 * Block Name: Product view
 * This is the template that displays the block product-view.
 *
 * @author Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>"> <!-- Porduct view block -->
	<?php
	$style = get_field('style');
	$section_title = get_field('section_title');
	$section_content = get_field('section_content');
	$section_button_link = get_field('section_button_link');
	
	$title_1 = get_field('title_1');
	$image_1 = get_field('image_1');
	$button_link_1 = get_field('button_link_1');

	$title_2 = get_field('title_2');
	$image_2 = get_field('image_2');
	$button_link_2 = get_field('button_link_2');

	$title_3 = get_field('title_3');
	$image_3 = get_field('image_3');
	$button_link_3 = get_field('button_link_3');

	$title_4 = get_field('title_4');
	$image_4 = get_field('image_4');
	$button_link_4 = get_field('button_link_4');
	?>
	<div class="product-view--background <?php if ($style == 'right') { echo 'white'; }; ?>">
		<?php if ($style == 'left') { ?>
			<img class="product-view--background--fade" src="<?php echo get_theme_mod( 'fade_image_block'); ?>">
		<?php }; ?>
		<div class="content-holder">
			<div class="wp-bootstrap-blocks-container container">
				<div class="wp-bootstrap-blocks-row row">
					<div class="col-12 col-md-6 d-flex <?php if ($style == 'right') { echo 'order-2'; }; ?>">
						<div class="m-auto product-view--content">
							<h1 class="<?php if ($style == 'left') { echo 'text-white'; }; ?> pb-3"><?php echo $section_title; ?></h1>
							<p class="<?php if ($style == 'left') { echo 'text-white'; }; ?> pb-5"><?php echo $section_content; ?></p>
							<li class="wp-block-button <?php if ($style == 'left') { echo 'is-style-fill-secondary'; }else{ echo 'is-style-outline'; }; ?>">
								<a class="wp-block-button__link" href="<?php echo $section_button_link; ?>"><?php _e('All products', 'maatwerkonline'); ?></a>
							</li>
						</div>
					</div>
					<div class="col-12 col-md-6 product-view--product-holder <?php if ($style == 'right') { echo 'order-1'; }; ?>">
						<div class="product-view--product-holder--product">
							<div class="product-1 product">
								<img class="product-fade" src="<?php echo get_theme_mod( 'blue_fade_image_block'); ?>"></img>
								<img class="mx-auto" src="<?php echo $image_1; ?>">
								<h3 class="mx-auto"><?php echo $title_1; ?></h3>
								<li class="wp-block-button is-style-fill mx-auto">
									<a class="wp-block-button__link" href="<?php echo $button_link_1; ?>"><?php _e('View product', 'maatwerkonline'); ?></a>
								</li>
							</div>
						</div>
						<div class="product-view--product-holder--product">
							<div class="product-1 product">
								<img class="product-fade" src="<?php echo get_theme_mod( 'blue_fade_image_block'); ?>"></img>
								<img class="mx-auto" src="<?php echo $image_2; ?>">
								<h3 class="mx-auto"><?php echo $title_2; ?></h3>
								<li class="wp-block-button is-style-fill mx-auto">
									<a class="wp-block-button__link" href="<?php echo $button_link_2; ?>"><?php _e('View product', 'maatwerkonline'); ?></a>
								</li>
							</div>
						</div>
						<div class="product-view--product-holder--product">
							<div class="product-1 product">
								<img class="product-fade" src="<?php echo get_theme_mod( 'blue_fade_image_block'); ?>"></img>
								<img class="mx-auto" src="<?php echo $image_3; ?>">
								<h3 class="mx-auto"><?php echo $title_3; ?></h3>
								<li class="wp-block-button is-style-fill mx-auto">
									<a class="wp-block-button__link" href="<?php echo $button_link_3; ?>"><?php _e('View product', 'maatwerkonline'); ?></a>
								</li>
							</div>
						</div>
						<div class="product-view--product-holder--product">
							<div class="product-1 product">
								<img class="product-fade" src="<?php echo get_theme_mod( 'blue_fade_image_block'); ?>"></img>
								<img class="mx-auto" src="<?php echo $image_4; ?>">
								<h3 class="mx-auto"><?php echo $title_4; ?></h3>
								<li class="wp-block-button is-style-fill mx-auto">
									<a class="wp-block-button__link" href="<?php echo $button_link_4; ?>"><?php _e('View product', 'maatwerkonline'); ?></a>
								</li>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>