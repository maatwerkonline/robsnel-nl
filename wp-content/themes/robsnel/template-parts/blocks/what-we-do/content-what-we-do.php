<?php
/**
 * Block Name: What we do
 * This is the template that displays the block what-we-do.
 *
 * @author Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>"> <!-- What we do block -->
	<?php
	$image = get_field('image');
	$title = get_field('title');
	$button = get_field('button_link');
	?>
	<div class="what-we-do--content">
		<img class="what-we-do--content--image" src="<?php echo $image; ?>">
		<img class="what-we-do--content--fade" src="<?php echo get_theme_mod( 'blue_fade_image_block'); ?>">
		<div class="what-we-do--content--text">
			<h3 class="my-auto text-white "><?php echo $title; ?></h3>
			<li class="wp-block-button is-style-fill-secondary homepage-title--button">
				<a class="wp-block-button__link" href="<?php echo $button; ?>"><?php _e('More info', 'maatwerkonline'); ?></a>
			</li>
		</div>
	</div>
</div>