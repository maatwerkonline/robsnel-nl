<?php
/**
 * Block Name: homepage-title
 * This is the template that displays the block homepage-title.
 *
 * @author Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>"> <!-- Homepage title block -->
	<?php
	$title = get_field('title');
	$content = get_field('content');
	$button_title = get_field('button_title');
	$button_link = get_field('button_link');
	$image = get_field('image');
	?>
	<div class="content-holder">
		<div class="wp-bootstrap-blocks-container container">
			<div class="wp-bootstrap-blocks-row row">
				<div class="col-5 homepage-title--content">
					<h1 class="text-white pb-5"><?php echo $title; ?></h1>
					<p class="text-white pb-4"><?php echo $content; ?></p>
					<li class="wp-block-button is-style-fill-secondary pb-3 homepage-title--button">
						<a class="wp-block-button__link" href="<?php echo $button_link; ?>"><?php echo $button_title; ?></a>
					</li>
				</div>
				<div class="col-7">
				</div>
			</div>
		</div>
	</div>
	<img class="homepage-title--image" src="<?php echo $image; ?>">
	<img class="homepage-title--fade" src="<?php echo get_theme_mod( 'fade_image_block'); ?>" >
</div>