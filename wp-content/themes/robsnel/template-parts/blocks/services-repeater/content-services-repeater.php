<?php

/**
 * Block Name: Services Repeater
 * This is the template that displays the block services-repeater.
 *
 * @author Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}
?>
 <div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?> services"> <!-- You may change this element to another element (for example blockquote for quotes) -->
	<?php if( have_rows('services') ): $i=0; ?>
		<?php  while( have_rows('services') ) : the_row(); $i++; ?>
			<?php if ($number % 2 == 0) : ?>
				<div class="service--even">
					<div class="row">
						<div class="col-12 col-xl-6">
							<?php 
								$featured_image_id = get_sub_field('featured_image');
								$featured_image = wp_get_attachment_image_src( $featured_image_id, 'medium');
							?>
							<img src="<?php echo $featured_image[0]; ?>">
						</div>	
						<div class="col-12 col-xl-6">
							<?php get_sub_field('title') ?>
							<?php get_sub_field('description') ?>
						</div>	
					</div>	
				</div>	
			<?php else: ?>
				<div class="service--odd">
					<div class="row">
						<div class="col-12 col-xl-6">
						</div>
						<div class="col-12 col-xl-6">
							<?php get_sub_field('title') ?>
						</div>		
					</div>	
				</div>	
			<?php endif; ?>
		<?php endwhile; ?>
	<?php endif; ?>
</div>