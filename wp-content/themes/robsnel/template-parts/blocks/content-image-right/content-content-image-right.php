<?php
/**
 * Block Name: Content Image Right
 * This is the template that displays the block content-image-right.
 *
 * @author Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?> content_image_right"> <!-- You may change this element to another element (for example blockquote for quotes) -->
	<div class="content-holder">
		<div class="row">
			<div class="col-12 col-xl-6 order-2 order-xl-1 my-auto text-blue">
				<h3>
					<?php echo get_field('title'); ?>
				</h3>
				<div>
					<?php echo get_field('content'); ?>
				</div>
			</div>
			<div class="col-12 col-xl-6 order-1 order-xl-2 content_image_right__image mb-4 mb-xl-0">
				<?php 
					$image_id = get_field('image');
					$image = wp_get_attachment_image_src( $image_id, 'large');
				?>
				<img src="<?php echo $image[0]; ?>">
			</div>
		</div>
	</div>
</div>