<div class="col-4">
    <a href="<?php echo get_permalink(); ?>">
        <div class="product-card text-center px-2 py-4 mb-3">
            <img class="product-card--image mx-auto" src="<?php echo get_the_post_thumbnail_url(); ?>">
            <h4 class="product-card--title text-blue mb-2"><?php echo get_the_title(); ?></h4>
            <p class="product-card--content text-blue mb-3">Ø 270/180</p>
            <li class="wp-block-button is-style-outline">
                <span class="wp-block-button__link"><?php _e('View product', 'maatwerkonline'); ?></span>
            </li>
        </div>
    </a>
</div>