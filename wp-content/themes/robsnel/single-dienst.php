<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header(); ?>

<?php 
if ( function_exists('yoast_breadcrumb') ) :  // If Yoast Breadcrumbs is enabled on /wp-admin/admin.php?page=wpseo_titles#top#breadcrumbs, show the breadcrumbs
    yoast_breadcrumb( '<p class="mb-0 py-3 breadcrumb" id="breadcrumbs">','</p>' );
endif;
?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
    <div class="content-holder service_single">
        <div class="row service_single__top_content">
            <div class="col-12 col-xl-5 order-3 order-xl-1">
                <h2 class="text-blue mb-4"><?php echo get_the_title(); ?></h2>
                <div class="text-blue">
                    <?php echo get_field('description'); ?>
                </div>   
            </div>  
            <div class="col-12 col-xl-1 order-2 order-xl-2"> 
            </div>   
            <div class="col-12 col-xl-6 order-1 order-xl-3">
                <div class="service_single__top_content__featured_image mb-5 mb-xl-0">
                    <img class="service_single__top_content__featured_image__image" src="<?php echo get_the_post_thumbnail_url(get_the_id(),'large'); ?>">
                    <img class="service_single__top_content__featured_image__overlay" src="<?php echo get_site_url(); ?>/wp-content/uploads/2022/01/blue-fade.png">
                </div>   
            </div>   
        </div>   
        <div class="row service_single__carousel_section">
            <div class="col-12 text-center">
                <h2 class="text-blue mb-5"><?php echo get_field('carousel_title'); ?></h2>
            </div>    
            <div class="col-12">
                <div class="service_single__carousel_section__carousel carousel" data-accessibility="0" data-adaptiveheight="0" data-autoplay="0" data-autoplayspeed="5000" data-arrows="1" data-centermode="0"  data-centerpadding="0" data-dots="0" data-effect="scroll" data-swipe="1" data-easing="linear" data-infinite="1" data-initialslide="1" data-pauseonhover="1" data-speed="300" data-variablewidth="0" data-slidestoscroll="1" data-slidestoshow="1" data-responsive="1" data-slidestoscroll_1190="1" data-slidestoshow_1190="1" data-slidestoscroll_768="1" data-slidestoshow_768="1" data-slidestoscroll_480="1" data-slidestoshow_480="1">
                    <?php
                        if( have_rows('carousel_images') ):
                            while( have_rows('carousel_images') ) : the_row();
                                $carousel_id = get_sub_field('image');
                                $carousel_image = wp_get_attachment_image_src( $carousel_id, 'large');
                                ?>
                                <div class="service_single__carousel_section__carousel__slider-image">
                                    <img src="<?php echo $carousel_image[0]; ?>">
                                </div>    
                                <?php
                            endwhile;
                        endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="service_single__editor_content">
        <?php the_content();?>
    </div>
<?php endwhile; endif;?>

<?php get_footer();
