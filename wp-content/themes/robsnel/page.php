<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header(); ?>

<?php if ( !is_front_page( ) ) :
	if ( function_exists('yoast_breadcrumb') ) :  // If Yoast Breadcrumbs is enabled on /wp-admin/admin.php?page=wpseo_titles#top#breadcrumbs, show the breadcrumbs
		yoast_breadcrumb( '<p class="mb-0 py-3 breadcrumb" id="breadcrumbs">','</p>' );
	endif;
endif;
?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
	<?php the_content();?>
<?php endwhile; endif;?>

<?php get_footer();
