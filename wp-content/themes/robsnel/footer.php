<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
				</div>
				<div class="footer-banner">
					<div class="footer-banner--content">
						<?php dynamic_sidebar( __('First Footer Widget', 'onm_textdomain') );?>
					</div>
					<img src="<?php echo get_theme_mod( 'fade_image_block'); ?>" class="footer-banner--fade">
				</div>
				<footer id="footer">
					<div class="content-holder">
						<div class="wp-bootstrap-blocks-container container">
							<div class="wp-bootstrap-blocks-row row">
								<div class="col-3">
									<div class="footer-image"><?php dynamic_sidebar( __('Second Footer Widget', 'onm_textdomain') );?></div>
								</div>
								<div class="col-2"><?php dynamic_sidebar( __('Third Footer Widget', 'onm_textdomain') );?></div>
								<div class="col-2"><?php dynamic_sidebar( __('Fourth Footer Widget', 'onm_textdomain') );?></div>
								<div class="col-2"><?php dynamic_sidebar( __('Fifth Footer Widget', 'onm_textdomain') );?></div>
								<div class="col-3"><?php dynamic_sidebar( __('Sixth Footer Widget', 'onm_textdomain') );?></div>

								<div class="col-12"><?php dynamic_sidebar( __('Seventh Footer Widget', 'onm_textdomain') );?></div>
							</div>
						</div>
					</div>
				</footer>

			</div> <!-- end #page-content-wrapper -->

		</div> <!-- end #wrapper -->

		<?php wp_footer(); ?>

	</body>
</html>
