<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<?php wp_head();?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	
	<?php if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) { gtm4wp_the_gtm_tag(); } ?>

	<div id="wrapper">
		<?php wp_nav_menu( array( 'menu' => 'mobile', 'theme_location' => 'mobile', 'container_id' => 'sidebar-wrapper', 'container_class' => 'bg-dark border-right', 'menu_class' => 'nav navbar-nav', 'fallback_cb' => false, 'walker' => new default_walker_nav_menu()) ); ?>

		<div id="page-content-wrapper">

			<?php if ( !is_page_template( 'page-leadpage.php' ) ) :

				get_template_part('template-parts/header');

			endif; ?>
			<div class="content-holder">
