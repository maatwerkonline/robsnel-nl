<?php

/**
 * Plugin Name: Maatwerk Online Snippets 
 * Description: Plugins that adds comonly used snippets on your website
 * Author: Maatwerk Online
 * Version: 1.0.0
 * Author URI: http://maatwerkonline.nl/
 */

defined('ABSPATH') or exit; // Exit if accessed directly

if ( !class_exists( 'Maatwerk_Online_Snippets' ) ) {
    class Maatwerk_Online_Snippets {

        static function init() {

            // Define constants to use in plugin.
            define('MOS_PLUGIN', __FILE__);
            define('MOS_PATH_URL', plugin_dir_path(__FILE__));
            define('MOS_URL', plugin_dir_url(__FILE__));
            define('MOS_BASENAME', plugin_basename(__FILE__));

            // Load textdomain for translations
            load_plugin_textdomain('mos', false, dirname(plugin_basename(__FILE__)) . '/languages');

            // Fetch main plugin functions and run them
            require_once('includes/functions.php');
            $mos_functions = new Maatwerk_Online_Snippets_Functions();
            $mos_functions->initialize_functions();
        }
    }

    Maatwerk_Online_Snippets::init();
}

/**
    * On activate plugin add the snippets with the comment "Default Active" to the active snippets array in the database.
*/
function mos_install_plugin(){

    // Fetch Snippet files from the theme folder and plugin and get active snippets from database
    $files_plugin = glob(plugin_dir_path(__FILE__).'snippets/*');
    $files_theme = glob(get_template_directory() .'/snippets/*');
    $files = array_merge($files_plugin, $files_theme);
    $options = get_option( 'snippets_name' );

    // Loop through files and check comments in top of the files
    foreach($files as $file) {
        $data = get_file_data($file, array('active'=>'Default Active', 'name' => 'Snippet Name') ); 
        if($data['active'] == 'true' ) {
            if (!empty($options)) {
                if (($key = array_search($file, $options)) !== false) {
                    unset($options[$key]);
                }
            } 
            array_push($options, $file);
        }
    }
    update_option( 'snippets_name', $options );
}
register_activation_hook( __FILE__, 'mos_install_plugin' );