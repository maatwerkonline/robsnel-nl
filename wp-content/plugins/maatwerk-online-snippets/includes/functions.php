<?php

defined('ABSPATH') or exit;

class Maatwerk_Online_Snippets_Functions {

  /**
    * Initialise all plugin related functions.
  */
  function initialize_functions() {
    add_action( 'admin_enqueue_scripts', array(&$this, 'mos_style') );
    add_action( 'admin_enqueue_scripts', array(&$this, 'mos_scripts') );
    add_action( 'admin_menu', array(&$this, 'mos_add_settings_page') );
    add_action( 'admin_init', array(&$this, 'mos_bulk_update_snippets') );
    add_action( 'admin_init', array(&$this, 'mos_update_single_snippet') );
    add_action( 'admin_init', array(&$this, 'mos_export_and_import_snippets') );
    $this->mos_load_snippets();
  }

  /**
    * Enqueue plugin stylesheet.
  */
  function mos_style() {
      wp_enqueue_style( 'mos-style', MOS_URL.'assets/style.css' );
  }

  /**
    * Enqueue plugin scripts.
  */
  function mos_scripts() {
    wp_enqueue_script( 'mos-scripts', MOS_URL.'assets/init.js' );
  }

  /**
    * Add the settings page to the back-end menu.
  */
  function mos_add_settings_page() {
    if ( current_user_can( 'administrator' ) )
    {
      add_options_page( 'Snippets', 'Snippets', 'manage_options', 'mos-plugin', array(&$this, 'mos_render_plugin_settings_page') );
    }
  }

  /**
    * Update the options snippets_name with the new active snippets when activating or deactivating snippets. 
  */
  function mos_bulk_update_snippets() {
    if( !empty($_POST['action'] ) && !empty( $_POST['snippets_name'] )) {

      // Fetch active snippets from database option and fetch checked 
      $options = array();
      $options = get_option( 'snippets_name' );
      $selected_snippets = array();
      $selected_snippets = $_POST['snippets_name'];

      // Update options array by comparing the selected entries with the array that is present in the database option 
      if($_POST['action'] == 'activate-selected') { // When activating snippets has been selected from the dropwdown
        if (!empty($options)) {
          foreach ($selected_snippets as $snippet) {
            if (($key = array_search($snippet, $options)) !== false) {
              $data = get_file_data($snippet, array('required'=>'Required') ); 
              if($data['required'] != 'true' ) {
                unset($options[$key]);
              }
            }
            array_push($options, $snippet);
          }
        } else {
          $options = $selected_snippets;
        }
      } elseif ($_POST['action'] == 'deactivate-selected') { // When deactivate snippets has been selected from the dropwdown
        foreach ($selected_snippets as $snippet) {
          if (($key = array_search($snippet, $options)) !== false) {
            $folder_name = basename($snippet);
            $file_path = $snippet.'/'.$folder_name.'.php';
            $data = get_file_data($file_path, array('required'=>'Required') );
            if($data['required'] != 'true' ) {
              unset($options[$key]);
            }
          }
        }
      }
      update_option( 'snippets_name', $options );
    }
  }

  /**
    * Activates or deactivates a single snippet when the deactivate/activate url is being clicked.
  */
  function mos_update_single_snippet() {
    if(!empty($_GET['activate'])) {
      $options = get_option( 'snippets_name' );
      if(!empty($options)) {
        array_push($options, $_GET['activate']);
        update_option( 'snippets_name', $options );
      } else {
        update_option( 'snippets_name', $_GET['activate'] );
      }
    }

    if(!empty($_GET['deactivate'])) {
      $options = get_option( 'snippets_name' );
      $value = $_GET['deactivate'];
      if (($key = array_search($value, $options)) !== false) {
          unset($options[$key]);
      }
      update_option( 'snippets_name', $options );
    }
  }

  /**
    * Export and import snippets from zip.
  */
  function mos_export_and_import_snippets() {

    // Export selected snippets to .zip file
    if(!empty($_POST['action']) && $_POST['action'] == 'export-selected') {
      $files = $_POST['snippets_name'];
      $zip = new ZipArchive();
      $zip_name = 'snippets-'.time().".zip";
      $zip->open($zip_name,  ZipArchive::CREATE);
      foreach ($files as $file) {
        if(file_exists($file)){
        $zip->addFromString(basename($file), file_get_contents($file));  
        }
      }
      $zip->close();
      while (ob_get_level()) {
        ob_end_clean();
      }
      ob_start();
      header($_SERVER['SERVER_PROTOCOL'].' 200 OK');
      header("Content-Type: application/zip");
      header("Content-Transfer-Encoding: Binary");
      header('Content-disposition: attachment; filename='.$zip_name.'');
      header('Content-Length: ' . filesize($zip_name));
      header('Content-type: application/zip');
      ob_flush();
      ob_clean(); 
      readfile($zip_name);
      exit();
    }

    // Install snippets from .zip.
    if(!empty($_POST['install-snippet-submit'])) {

      $filename = $_FILES["snippetzip"]["name"];
      $source = $_FILES["snippetzip"]["tmp_name"];
      $type = $_FILES["snippetzip"]["type"];

      $name = explode(".", $filename);
      $accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
      foreach($accepted_types as $mime_type) {
          if($mime_type == $type) {
              $okay = true;
              break;
          } 
      }

      $continue = strtolower($name[1]) == 'zip' ? true : false;
      if(!$continue) {
          $message = __('The file you are trying to upload is not a .zip file. Please try again.','mos');
          die();
      }

      $path = get_template_directory().'/snippets/';
      $filenoext = basename ($filename, '.zip');
      $filenoext = basename ($filenoext, '.ZIP');

      $targetdir = $path; // target directory
      $targetzip = $path . $filenoext.'.zip'; // target zip file

      if(move_uploaded_file($source, $targetzip)) {
        $zip = new ZipArchive();
        $zip->open($targetzip);
        $zip->extractTo($targetdir);
        $zip->close();
        unlink($targetzip);
      }
    }
  }

  /**
    * Generate the content of the settings page with the status of the snippets.
  */
  function mos_render_plugin_settings_page() {
    ?>
    <div class="wrap">
      <h2 class="snippets-plugin-title"><?php _e('Snippets','mos') ?></h2>
      <a href="" class="upload-view-toggle-snippets page-title-action" role="button" aria-expanded="true"><span class="upload"><?php _e('Upload snippets','mos') ?></span></a>
      <?php 
          // Get active snippets to use on settings page.
          $options = get_option( 'snippets_name' );
      ?>
      <div class="upload-plugin-wrap">
        <div class="upload-plugin">
          <p class="install-help"><?php _e('Upload a zip file withs your snippets included.','mos') ?></p>
          <form method="post" enctype="multipart/form-data" class="wp-upload-form" action="<?php echo get_site_url(); ?>/wp-admin/options-general.php?page=mos-plugin">
            <input type="file" id="snippetzip" name="snippetzip" accept=".zip">
            <input type="submit" name="install-snippet-submit" id="install-snippet-submit" class="button" value="<?php _e('Install now','mos') ?>" disabled="">	
          </form>
        </div>
      </div>
      <form action="<?php echo get_site_url(); ?>/wp-admin/options-general.php?page=mos-plugin" method="post">
        <?php 

          // Fetch snippets from plugin folder and theme folder
          $files_plugin = glob(MOS_PATH_URL .'snippets/*');
          $files_theme = glob(get_template_directory() .'/snippets/*');
          $files = array_merge($files_plugin, $files_theme);

          // Count active and inactive snippets
          $active_count = 0;
          $inactive_count = 0;

          foreach($files as $file) :
            $folder_name = basename($file);
            $file_path = $file.'/'.$folder_name.'.php';
            $data = get_file_data($file_path, array('ver'=>'Version', 'desc'=>'Description', 'name'=>'Snippet Name', 'dependent'=>'Dependent Plugin', 'required'=>'Required' ) ); 
            
            // Don't count if dependent account is not active
            if (in_array($data['dependent'], get_option('active_plugins')) || is_plugin_active_for_network($data['dependent']) || $data['required'] == "true") {
              if (in_array($file, $options)) {
                $active_count++;
              } else {
                $inactive_count++;
              }
            }
          endforeach;

          $total = $active_count + $inactive_count;
        ?>
        <ul class="subsubsub">
          <li class="all"><a href="<?php echo get_site_url(); ?>/wp-admin/options-general.php?page=mos-plugin" class="<?php if(empty($_GET['status'])) { echo 'current'; } ?>" aria-current="page"><?php _e('All','mos') ?> <span class="count">(<?php echo $total; ?>)</span></a> |</li>
          <li class="active"><a href="<?php echo get_site_url(); ?>/wp-admin/options-general.php?page=mos-plugin&status=active" class="<?php if($_GET['status'] == 'active' ) { echo 'current'; } ?>" ><?php _e('Active','mos') ?> <span class="count">(<?php echo $active_count; ?>)</span></a> |</li>
          <li class="inactive"><a href="<?php echo get_site_url(); ?>/wp-admin/options-general.php?page=mos-plugin&status=inactive" class="<?php if($_GET['status'] == 'inactive' ) { echo 'current'; } ?>"><?php _e('Inactive','mos') ?> <span class="count">(<?php echo $inactive_count; ?>)</span></a></li>
        </ul>
        <div class="tablenav top">
          <div class="alignleft actions bulkactions">
            <label for="bulk-action-selector-top" class="screen-reader-text"><?php _e('Bulk Actions','mos') ?></label>
            <select name="action" id="bulk-action-selector-top">
              <option value="-1"><?php _e('Bulk Actions','mos') ?></option>
              <option value="activate-selected"><?php _e('Activate','mos') ?></option>
              <option value="deactivate-selected"><?php _e('Deactivate','mos') ?></option>
              <option value="export-selected"><?php _e('Export','mos') ?></option>
            </select>
            <input type="submit" name="submit" class="button action" value="<?php _e('Apply changes','mos') ?>">
          </div>
        </div>
        <table class="wp-list-table widefat plugins">
          <thead>
            <tr>
              <td id="cb" class="manage-column column-cb check-column"><label class="screen-reader-text" for="cb-select-all-1"><?php _e('Sellect all','mos') ?></label><input id="cb-select-all-1" type="checkbox"></td>
              <th scope="col" id="name" class="manage-column column-name column-primary"><?php _e('Snippet','mos') ?></th>
              <th scope="col" id="description" class="manage-column column-description"><?php _e('Description','mos') ?></th>	
            </tr>
          </thead>
          <tbody id="the-list"> 
          <?php 
            foreach($files as $file) :
              $folder_name = basename($file);
              $file_path = $file.'/'.$folder_name.'.php';

              // Get comment data from top of folder
              $data = get_file_data($file_path, array('ver'=>'Version', 'desc'=>'Description', 'name'=>'Snippet Name', 'required'=>'Required', 'dependent'=>'Dependent Plugin' , 'author'=>'Author' , 'author-uri'=>'Author URI', 'snippet-uri'=>'Snippet URI' ) ); 

              // register active status for input and row
              $active = "";

              if (in_array($file, $options) || $data['required'] == "true" ) {
                $active = "active";
              } else {
                $active = "inactive";
              }

              // Display row for each Snippet
              if (in_array($data['dependent'], get_option('active_plugins')) || is_plugin_active_for_network($data['dependent']) || empty($data['dependent']) ) :
                ?>
                <tr style="<?php if($_GET['status'] == 'active' && $active == 'inactive') { echo 'display:none;'; } elseif( $_GET['status'] == 'inactive' && $active == 'active') { echo 'display:none;'; } ?>" class="<?php echo $active; ?>">
                  <th scope="row" class="check-column">
                      <input type="checkbox" name="snippets_name[]" value="<?php echo $file; ?>">
                  </th>
                  <td class="snippet-title column-primary">
                    <strong><?php echo $data['name']; ?></strong>
                    <div class="row-actions visible">
                      <?php if($data['required'] != 'true') : ?>
                        <?php if ($active != "active") : ?>
                          <span class="activate"><a href="<?php echo get_site_url(); ?>/wp-admin/options-general.php?page=mos-plugin&activate=<?php echo urlencode($file);  ?>" class="edit" aria-label="<?php _e('Activate','mos') ?> <?php echo $data['name']; ?>"><?php _e('Activate','mos') ?></a></span>  
                        <?php else: ?>
                          <span class="deactivate"><a href="<?php echo get_site_url(); ?>/wp-admin/options-general.php?page=mos-plugin&deactivate=<?php echo urlencode($file); ?>" class="edit" aria-label="<?php _e('Deactivate','mos') ?> <?php echo $data['name']; ?>"><?php _e('Deactivate','mos') ?></a></span> 
                        <?php endif; ?> 
                      <?php endif; ?> 
                      <?php if($data['required'] == 'true') : ?>
                        <a id="deactivate-gutenberg" aria-label="Gutenberg deactiveren" style="position: relative; color: #82878c; cursor: default; font-size: 13px;"><span class="locked-indicator-icon" aria-hidden="true"></span><?php _e('Deactivate','mos') ?></a>
                      <?php endif; ?> 
                    </div>
                  </td>
                  <td class="column-description desc">
                    <div class="snippet-description">
                      <p><?php echo $data['desc']; ?></p>
                    </div>
                  <div class="active second plugin-version-author-uri"><?php _e('Version: ','mos') ?><?php echo $data['ver']; ?> | <?php _e('By: ','mos') ?> <?php if(!empty($data['author-uri'])) : ?> <a href="<?php echo $data['author-uri']; ?>"> <?php endif; ?> <?php echo $data['author']; ?> <?php if(!empty($data['author-uri'])) : ?></a><?php endif; ?> <?php if(!empty($data['snippet-uri'])) : ?> | <a href="<?php echo $data['snippet-uri']; ?>"> <?php _e('Visit Snippet Site','mos') ?> </a> <?php endif; ?></div>
                  </td>
                </tr>
                <?php  
              endif;
            endforeach;
            ?>
            </tbody>
            <tfoot>
              <tr>
                <td id="cb" class="manage-column column-cb check-column"><label class="screen-reader-text" for="cb-select-all-1"><?php _e('Sellect all','mos') ?></label><input id="cb-select-all-1" type="checkbox"></td>
                <th scope="col" id="name" class="manage-column column-name column-primary"><?php _e('Snippet','mos') ?></th>
                <th scope="col" id="description" class="manage-column column-description"><?php _e('Description','mos') ?></th>	
              </tr>
            </tfoot>
          </table>
          <div class="tablenav bottom">
            <div class="alignleft actions bulkactions">
            <label for="bulk-action-selector-bottom" class="screen-reader-text"><?php _e('Bulk Actions','mos') ?></label>
              <select name="action2" id="bulk-action-selector-bottom">
                <option value="-1"><?php _e('Bulk Actions','mos') ?></option>
                <option value="activate-selected"><?php _e('Activate','mos') ?></option>
                <option value="deactivate-selected"><?php _e('Deactivate','mos') ?></option>
              </select>
              <input type="submit" name="submit" class="button action" value="<?php _e('Apply changes','mos') ?>">
            </div>
          </div>
      </form>
    </div>
    <?php
  }
  
  /**
    * Load the active snippets
  */
  function mos_load_snippets() {
    
    // Fetch Snippet files from the theme folder and plugin and get active snippets from database
    $options = array();
    $files_plugin = glob(MOS_PATH_URL .'snippets/*');
    $files_theme = glob(get_template_directory() .'/snippets/*');
    $files = array_merge($files_plugin, $files_theme);
    if(!empty(get_option( 'snippets_name' ))) {
    $options = get_option( 'snippets_name' );
    }

    // Loop through all the files and run the snippet if the snippet is present in the option snippet_name or if the parameter required is present in the file
    foreach($files as $file) {
      $folder_name = basename($file);
      $file_path = $file.'/'.$folder_name.'.php';

      // Get comment data from top of folder
      $data = get_file_data($file_path, array('name'=>'Snippet Name', 'dependent'=>'Dependent Plugin', 'required'=>'Required' ) ); 

      //If required exists run code regardless of database active status and only run if dependent plugin is active or no dependent plugin has been assigned 
      if($data['required'] == "true" && (in_array($data['dependent'], get_option('active_plugins')) || is_plugin_active_for_network($data['dependent']) || empty($data['dependent']))) {
        include($file_path);
      } elseif (in_array($file, $options)) {

        // Don't run snippet if dependent plugin is not active
        if (in_array($data['dependent'], get_option('active_plugins')) || is_plugin_active_for_network($data['dependent']) || empty($data['dependent']) ) {
          include($file_path);
        }
      }
    }
  }
}