��          T       �       �   �   �        (   +  �   T            �  3  �   �     ~  (   �  �   �     f     �   Cannot activate WooCommerce Van Der Meer Shipping Plugin because woocommerce is not found.<br />Please install and activate woocommerce first Maatwerk Online WooCommerce Van Der Meer Shipping Plugin WooCommerce Van Der Meer Shipping Plugin should create an XML file of each order which is created. And it should also create an XML file of each order which will be canceled. Woocommerce not found http://maatwerkonline.nl/ Project-Id-Version: WooCommerce Van Der Meer Shipping Plugin
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-09-24 13:35+0000
PO-Revision-Date: 2020-09-24 13:36+0000
Last-Translator: Maatwerk Online
Language-Team: Nederlands
Language: nl_NL
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.3; wp-5.5.1 Kan WooCommerce Van Der Meer Shipping Plugin niet activeren omdat woocommerce niet gevonden is. <br /> Installeer en activeer eerst woocommerce Maatwerk Online WooCommerce Van Der Meer Shipping Plugin WooCommerce Van Der Meer Shipping Plugin should create an XML file of each order which is created. And it should also create an XML file of each order which will be canceled. 
Woocommerce niet gevonden http://maatwerkonline.nl/ 