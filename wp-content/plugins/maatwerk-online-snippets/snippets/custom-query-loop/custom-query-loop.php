<?php

/**
 * Snippet Name: Register Custom Query Loop Block
 * Description: Replaces the default Custom Query Loop block
 * Version: 1.1
 * Author: Frank van 't Hof
 * Required: true
 * Default Active: true
 */


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function mo_loop_scripts() {
    wp_enqueue_script('ajax', MOS_URL.'snippets/custom-query-loop/assets/ajax.js');
    wp_enqueue_script('loop-js', MOS_URL.'snippets/custom-query-loop/assets/init.js');
};
add_action('init', 'mo_loop_scripts');


// Needed for back-end ajax, still in development.
function block_editor_loop_scripts() {
    wp_enqueue_script('ajax', MOS_URL.'snippets/custom-query-loop/assets/ajax.js', array( 'wp-blocks', 'wp-dom' ), filemtime( MOS_URL.'snippets/custom-query-loop/assets/ajax.js' ), true );
	wp_enqueue_script('loop-js', MOS_URL.'snippets/custom-query-loop/assets/init.js', array( 'wp-blocks', 'wp-dom' ), filemtime( MOS_URL.'snippets/custom-query-loop/assets/init.js' ), true );
}
// add_action( 'enqueue_block_editor_assets', 'block_editor_loop_scripts' );


function mo_register_query_loop_block() {

    // check function exists.
    if( function_exists('acf_register_block_type') ) {

        // register a testimonial block.
        acf_register_block_type(array(
            'name'              => 'queryloop',
            'title'             => __('Query Loop'),
            'description'       => __('A custom query loop block.'),
            'icon'              => '<svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" role="img" aria-hidden="true" focusable="false"><path d="M18.1823 11.6392C18.1823 13.0804 17.0139 14.2487 15.5727 14.2487C14.3579 14.2487 13.335 13.4179 13.0453 12.2922L13.0377 12.2625L13.0278 12.2335L12.3985 10.377L12.3942 10.3785C11.8571 8.64997 10.246 7.39405 8.33961 7.39405C5.99509 7.39405 4.09448 9.29465 4.09448 11.6392C4.09448 13.9837 5.99509 15.8843 8.33961 15.8843C8.88499 15.8843 9.40822 15.781 9.88943 15.5923L9.29212 14.0697C8.99812 14.185 8.67729 14.2487 8.33961 14.2487C6.89838 14.2487 5.73003 13.0804 5.73003 11.6392C5.73003 10.1979 6.89838 9.02959 8.33961 9.02959C9.55444 9.02959 10.5773 9.86046 10.867 10.9862L10.8772 10.9836L11.4695 12.7311C11.9515 14.546 13.6048 15.8843 15.5727 15.8843C17.9172 15.8843 19.8178 13.9837 19.8178 11.6392C19.8178 9.29465 17.9172 7.39404 15.5727 7.39404C15.0287 7.39404 14.5066 7.4968 14.0264 7.6847L14.6223 9.20781C14.9158 9.093 15.2358 9.02959 15.5727 9.02959C17.0139 9.02959 18.1823 10.1979 18.1823 11.6392Z"></path></svg>',
            'render_callback'   => 'render_query_loop',
            'category'          => 'formatting',
        ));

        // register default values for the query loop 
        register_field_group(array (
            'key' => 'loopfields',
            'title' => 'Loop Fields',
            'fields' => array(
                array(
                    'key' => 'looptype',
                    'name' => 'looptype',
                    'label' => __('Select Loop Type', 'mos'),
                    'instructions' => '',
                    'choices' => array(
                        'pagination'	=> __('Pagination', 'mos'),
                        'carousel'	=> __('Carousel', 'mos'),
                    ),
                    'type' => 'select',
                ),
                array(
                    'key' => 'specificposts',
                    'name' => 'specificposts',
                    'label' => __('Select Specific Posts', 'mos'),
                    'instructions' => '',
                    'choices' => array(
                        ''	=> __('Select an option', 'mos'),
                        'no'	=> __('No', 'mos'),
                        'yes'	=> __('Yes', 'mos'),
                    ),
                    'type' => 'select',
                ),
                array(
                    'key' => 'inheritquery',
                    'name' => 'inheritquery',
                    'label' => __('Inherit Query', 'mos'),
                    'instructions' => '',
                    'type' => 'checkbox',
                    'choices' => array(
                        'inherit'	=> __('Inherit Query', 'mos')
                    ),
                    'layout' => 'vertical',
                    'conditional_logic' => array (
                        array (
                            array (
                                'field' => 'specificposts',
                                'operator' => '==',
                                'value' => 'no',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'posttype',
                    'name' => 'posttype',
                    'label' => __('Post Type', 'mos'),
                    'instructions' => '',
                    'type' => 'select',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'inheritquery',
                            'operator' => '!=',
                            'value' => 'inherit',
                            ),
                            array (
                                'field' => 'specificposts',
                                'operator' => '!=',
                                'value' => '',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'selectspecificposts',
                    'name' => 'selectspecificposts',
                    'label' => __('Select Posts To Show', 'mos'),
                    'type' => 'post_object',
                    'multiple' => 1,
                    'ui' => 1,
                    'ajax' => 1,
                    'return_format' => 'id',
                    'conditional_logic' => array (
                        array (
                            array (
                                'field' => 'specificposts',
                                'operator' => '==',
                                'value' => 'yes',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'termfilter',
                    'name' => 'termfilter',
                    'label' => __('Show Term Filter', 'mos'),
                    'instructions' => '',
                    'type' => 'checkbox',
                    'choices' => array(
                        'show'	=> __('Show Filter', 'mos')
                    ),
                    'layout' => 'vertical',
                    'conditional_logic' => array (
                        array (
                            array (
                                'field' => 'specificposts',
                                'operator' => '==',
                                'value' => 'no',
                            ),
                        ),
                    ),
                    
                ),
                array(
                    'key' => 'taxonomyfilter',
                    'name' => 'taxonomyfilter',
                    'label' => __('Select Taxonomy To Show Terms Of', 'mos'),
                    'type' => 'select',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'termfilter',
                            'operator' => '!=',
                            'value' => '',
                            ),
                            array (
                                'field' => 'specificposts',
                                'operator' => '==',
                                'value' => 'no',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'taxonomy',
                    'name' => 'taxonomy',
                    'label' => __('Taxonomy', 'mos'),
                    'instructions' => __('Leave empty to ignore', 'mos'),
                    'type' => 'select',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'inheritquery',
                            'operator' => '!=',
                            'value' => 'inherit',
                            ),
                            array (
                                'field' => 'termfilter',
                                'operator' => '!=',
                                'value' => 'show',
                            ),
                            array (
                                'field' => 'specificposts',
                                'operator' => '==',
                                'value' => 'no',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'termid',
                    'name' => 'termid',
                    'label' => __('Term ID', 'mos'),
                    'instructions' => __('Leave empty to ignore', 'mos'),
                    'type' => 'text',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'taxonomy',
                            'operator' => '!=',
                            'value' => 'select',
                            ),
                            array (
                                'field' => 'inheritquery',
                                'operator' => '!=',
                                'value' => 'inherit',
                            ),
                            array (
                                'field' => 'termfilter',
                                'operator' => '!=',
                                'value' => 'show',
                            ),
                            array (
                                'field' => 'specificposts',
                                'operator' => '==',
                                'value' => 'no',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'infinite',
                    'name' => 'infinite',
                    'label' => __('Infinite Carousel', 'mos'),
                    'instructions' => '',
                    'type' => 'checkbox',
                    'choices' => array(
                        '1'	=> __('Activate', 'mos')
                    ),
                    'layout' => 'vertical',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'looptype',
                            'operator' => '==',
                            'value' => 'carousel',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'autoplay',
                    'name' => 'autoplay',
                    'label' => __('Autoplay Carousel', 'mos'),
                    'instructions' => '',
                    'type' => 'checkbox',
                    'choices' => array(
                        '1'	=> __('Activate', 'mos')
                    ),
                    'layout' => 'vertical',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'looptype',
                            'operator' => '==',
                            'value' => 'carousel',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'autoplayspeed',
                    'name' => 'autoplayspeed',
                    'default_value' => '8000',
                    'label' => __('Auto Play Speed In Ms', 'mos'),
                    'instructions' => '',
                    'type' => 'text',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'looptype',
                            'operator' => '==',
                            'value' => 'carousel',
                            ),
                            array (
                            'field' => 'autoplay',
                            'operator' => '==',
                            'value' => '1',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'pauseonhover',
                    'name' => 'pauseonhover',
                    'label' => __('Pause On Hover Carousel', 'mos'),
                    'instructions' => '',
                    'type' => 'checkbox',
                    'choices' => array(
                        '1'	=> __('Activate', 'mos')
                    ),
                    'layout' => 'vertical',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'looptype',
                            'operator' => '==',
                            'value' => 'carousel',
                            ),
                            array (
                                'field' => 'autoplay',
                                'operator' => '==',
                                'value' => '1',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'centermode',
                    'name' => 'centermode',
                    'label' => __('Center Mode Carousel', 'mos'),
                    'instructions' => '',
                    'type' => 'checkbox',
                    'choices' => array(
                        '1'	=> __('Activate', 'mos')
                    ),
                    'layout' => 'vertical',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'looptype',
                            'operator' => '==',
                            'value' => 'carousel',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'arrows',
                    'name' => 'arrows',
                    'label' => __('Show Arrows Carousel', 'mos'),
                    'instructions' => '',
                    'type' => 'checkbox',
                    'choices' => array(
                        '1'	=> __('Activate', 'mos')
                    ),
                    'layout' => 'vertical',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'looptype',
                            'operator' => '==',
                            'value' => 'carousel',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'dots',
                    'name' => 'dots',
                    'label' => __('Show Dots Carousel', 'mos'),
                    'instructions' => '',
                    'type' => 'checkbox',
                    'choices' => array(
                        '1'	=> __('Activate', 'mos')
                    ),
                    'layout' => 'vertical',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'looptype',
                            'operator' => '==',
                            'value' => 'carousel',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'slidestoshow',
                    'name' => 'slidestoshow',
                    'label' => __('Slides To Show Carousel', 'mos'),
                    'instructions' => '',
                    'default_value' => '3',
                    'type' => 'number',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'looptype',
                            'operator' => '==',
                            'value' => 'carousel',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'slidestoscroll',
                    'name' => 'slidestoscroll',
                    'default_value' => '1',
                    'label' => __('Slides To Scroll Carousel', 'mos'),
                    'instructions' => '',
                    'type' => 'number',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'looptype',
                            'operator' => '==',
                            'value' => 'carousel',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'slidestoshow1190',
                    'name' => 'slidestoshow1190',
                    'label' => __('Slides To Show Carousel 1190px', 'mos'),
                    'instructions' => '',
                    'type' => 'number',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'looptype',
                            'operator' => '==',
                            'value' => 'carousel',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'slidestoscroll1190',
                    'name' => 'slidestoscroll1190',
                    'label' => __('Slides To Scroll Carousel 1190px', 'mos'),
                    'instructions' => '',
                    'type' => 'number',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'looptype',
                            'operator' => '==',
                            'value' => 'carousel',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'slidestoshow768',
                    'name' => 'slidestoshow768',
                    'label' => __('Slides To Show Carousel 768px', 'mos'),
                    'instructions' => '',
                    'type' => 'number',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'looptype',
                            'operator' => '==',
                            'value' => 'carousel',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'slidestoscroll768',
                    'name' => 'slidestoscroll768',
                    'label' => __('Slides To Scroll Carousel 768px', 'mos'),
                    'instructions' => '',
                    'type' => 'number',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'looptype',
                            'operator' => '==',
                            'value' => 'carousel',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'slidestoshow480',
                    'name' => 'slidestoshow480',
                    'label' => __('Slides To Show Carousel 480px', 'mos'),
                    'instructions' => '',
                    'type' => 'number',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'looptype',
                            'operator' => '==',
                            'value' => 'carousel',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'slidestoscroll480',
                    'name' => 'slidestoscroll480',
                    'label' => __('Slides To Scroll Carousel 480px', 'mos'),
                    'instructions' => '',
                    'type' => 'number',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'looptype',
                            'operator' => '==',
                            'value' => 'carousel',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'postsperpage',
                    'name' => 'postsperpage',
                    'label' => __('Posts Per Page', 'mos'),
                    'instructions' => '',
                    'type' => 'number',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'looptype',
                            'operator' => '!=',
                            'value' => 'carousel',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'orderby',
                    'name' => 'orderby',
                    'label' => __('Order By', 'mos'),
                    'instructions' => '',
                    'choices' => array(
                        'name'	=> __('Name', 'mos'),
                        'date'	=> __('Date', 'mos'),
                        'id'	=> __('ID', 'mos'),
                        'rand'	=> __('Random', 'mos')
                    ),
                    'type' => 'select',
                ),
                array(
                    'key' => 'order',
                    'name' => 'order',
                    'label' => __('Order', 'mos'),
                    'instructions' => '',
                    'choices' => array(
                        'ASC'	=> __('ASC', 'mos'),
                        'DESC'	=> __('DESC', 'mos'),
                    ),
                    'type' => 'select',
                ),
                array(
                    'key' => 'templatepart',
                    'name' => 'templatepart',
                    'label' => __('Template Part', 'mos'),
                    'instructions' => __('Select a template part to use', 'mos'),
                    'type' => 'select',
                ),
                array(
                    'key' => 'row',
                    'name' => 'row',
                    'label' => __('Place Template Parts In A Row?', 'mos'),
                    'instructions' => '',
                    'type' => 'checkbox',
                    'choices' => array(
                        'row'	=> __('Place In Row', 'mos')
                    ),
                    'layout' => 'vertical',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'carousel',
                            'operator' => '!=',
                            'value' => 'carousel',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'paginationurl',
                    'name' => 'paginationurl',
                    'label' => __('URL Variable Used For Pagination', 'mos'),
                    'instructions' => '',
                    'type' => 'text',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'carousel',
                            'operator' => '!=',
                            'value' => 'carousel',
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'ajaxify',
                    'name' => 'ajaxify',
                    'label' => __('Ajaxify Loop?', 'mos'),
                    'instructions' => '',
                    'type' => 'checkbox',
                    'choices' => array(
                        'ajaxify'	=> __('Ajaxify', 'mos')
                    ),
                    'layout' => 'vertical',
                ),
                array(
                    'key' => 'loader',
                    'name' => 'loader',
                    'label' => __('Select A Loader Image', 'mos'),
                    'instructions' => '',
                    'type' => 'image',
                    'return_format' => 'id',
                    'layout' => 'vertical',
                    'conditional_logic' => array (
                        array (
                            array (
                            'field' => 'ajaxify',
                            'operator' => '==',
                            'value' => 'ajaxify',
                            ),
                        ),
                    ),
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'block',
                        'operator' => '==',
                        'value' => 'acf/queryloop'
                    ),
                ),
            ),
            
        ));

        function acf_load_post_type_choices( $field ) {
            $types = get_post_types( [], 'objects' );   
            $i = 0;
            $values = array();
            foreach ( $types as $type ) {
                $values = array_merge($values, array("".$type->name.""=>"".$type->name.""));
            }
            
            $field['choices'] = $values;
            // return the field
            return $field;
            
        }
        
        add_filter('acf/load_field/name=posttype', 'acf_load_post_type_choices');

        function acf_load_taxonomy_choices( $field ) {
            $types = get_taxonomies( [], 'objects' );   
            $i = 0;
            $values = array();
            $values = array_merge($values, array("select"=>"".__('Select', 'mos').""));
            foreach ( $types as $type ) {
                $values = array_merge($values, array("".$type->name.""=>"".$type->name.""));
            }
            
            $field['choices'] = $values;
            // return the field
            return $field;
            
        }
        
        add_filter('acf/load_field/name=taxonomy', 'acf_load_taxonomy_choices');

        function acf_load_taxonomyfilter_choices( $field ) {
            $types = get_taxonomies( [], 'objects' );   
            $i = 0;
            $values = array();
            $values = array_merge($values, array("select"=>"".__('Select', 'mos').""));
            foreach ( $types as $type ) {
                $values = array_merge($values, array("".$type->name.""=>"".$type->name.""));
            }
            
            $field['choices'] = $values;
            // return the field
            return $field;
            
        }
        
        add_filter('acf/load_field/name=taxonomyfilter', 'acf_load_taxonomyfilter_choices');

        function acf_load_template_part_choices( $field ) {
            $files_theme = glob(get_template_directory() .'/template-parts/loops/*');
            $values = array();
            $values = array_merge($values, array("select"=>"".__('Select', 'mos').""));
            foreach ($files_theme as $file ) {
                $folder_name = basename($file);
                $values = array_merge($values, array("".$file.""=>"".$folder_name.""));
            }
            
            $field['choices'] = $values;
            // return the field
            return $field;
        }

        add_filter('acf/load_field/name=templatepart', 'acf_load_template_part_choices');
    }
}
add_action( 'init', 'mo_register_query_loop_block' );


/**
 * Query loop Block Callback Function.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

function render_query_loop( $block, $content = '', $is_preview = false, $post_id = 0 ) {

    // Load selected values
    $looptype = get_field('looptype');
    $inherit_query = get_field('inheritquery');
    $post_type = get_field('posttype');
    $taxonomy = get_field('taxonomy');
    $term_id = get_field('termid');
    $taxonomyfilter = get_field('taxonomyfilter');
    $termfilter = get_field('termfilter');
    $post_per_page = get_field('postsperpage');
    $order_by = get_field('orderby');
    $order = get_field('order');
    $pagination_url = get_field('paginationurl');
    $template_part = get_field('templatepart');
    $row = get_field('row');
    $ajaxify = get_field('ajaxify');
    $loader = get_field('loader');
    $selectspecificposts = get_field('selectspecificposts');
   
    if($looptype == "carousel") {
        $carousel = 1;
    }
    
    if(!empty($carousel)) {
        $infinite = get_field('infinite');
        $autoplay = get_field('autoplay');
        $autoplayspeed = get_field('autoplayspeed');
        $arrows = get_field('arrows');
        $dots = get_field('dots');
        $pauseonhover = get_field('pauseonhover');
        $slidestoshow = get_field('slidestoshow');
        $slidestoscroll = get_field('slidestoscroll');
        $slidestoshow1190 = get_field('slidestoshow1190');
        $slidestoscroll1190 = get_field('slidestoscroll1190');
        $slidestoshow768 = get_field('slidestoshow768');
        $slidestoscroll768 = get_field('slidestoscroll768');
        $slidestoshow480 = get_field('slidestoshow480');
        $slidestoscroll480 = get_field('slidestoscroll480');
        $centermode = get_field('centermode');
    } else {
        $autoplay = 0;
        $autoplayspeed = 0;
        $arrows = 0;
        $dots = 0;
        $pauseonhover = 0;
        $slidestoshow = 0;
        $slidestoscroll = 0;
        $slidestoshow1190 = 0;
        $slidestoscroll1190 = 0;
        $slidestoshow768 = 0;
        $slidestoscroll768 = 0;
        $slidestoshow480 = 0;
        $slidestoscroll480 = 0;
        $centermode = 0;
    }

    $folder_name = basename($template_part);

    if(empty($term_id) && isset($_GET['term'])) {
        $term_id = $_GET['term'];
    }

    if($inherit_query[0] !== 'inherit') { 
        if(!empty($_GET[$pagination_url])) {
            $current_page = $_GET[$pagination_url];
        } else {
            $current_page = 1;
        }
        $args = array(  
            'post_type' => $post_type,
            'post_status' => 'publish',
            'posts_per_page' =>  $post_per_page, 
            'paged' =>  $current_page,
            'orderby' => $order_by, 
            'order' => $order,
            'post__in' => $selectspecificposts,
        );
        if(!empty($taxonomy) && $taxonomy != 'select') {
            $tax = array( 'tax_query' => array(
                    array (
                        'taxonomy' => $taxonomy,
                        'field'   => 'term_id',
                        'terms'   => $term_id,
                        'exclude' => array('1')
                    )
                ),
            );
            $args = array_merge($args, $tax);
        }
        if(!empty($_GET['term'])) {
            $tax = array( 'tax_query' => array(
                    array (
                        'taxonomy' => $taxonomyfilter,
                        'field' => 'term_id',
                        'terms' => $_GET['term'],
                        'exclude' => array('1')
                    )
                ),
            );
            $args = array_merge($args, $tax);
        }

        $loop = new WP_Query( $args ); 

        if ($loop->have_posts()) : ?>
            <div class="loop-container <?php echo 'container-'.$pagination_url; ?>">
                <?php if($termfilter[0]) : ?>
                    <div class="terms py-5">
                        <a href="<?php echo get_permalink(); ?>?term=" class="terms__block<?php if(empty($_GET['term'])) : ?>--active <?php endif; ?>">
                            <?php _e('All Categories','mos')  ?>
                        </a>
                        <?php 
                            $categories = get_terms($taxonomyfilter);
                            foreach ($categories as $category) :
                                if ($category->term_id != 1) :
                                ?>
                                    <a href="<?php echo get_permalink(); ?>?term=<?php echo $category->term_id; ?>" data-term="<?php echo $category->term_id; ?>" class="terms__block<?php if($_GET['term'] == $category->term_id) : ?>--active <?php endif; ?>">
                                        <?php echo $category->name ?>
                                    </a>
                                <?php
                                endif;
                            endforeach;
                        ?>
                    </div>    
                <?php endif; ?>
                <div class="wp-bootstrap-blocks-container container">
                    <div class="wp-bootstrap-blocks-row row">
                        <div class="col-3">
                            <h4>Filter</h4>
                            <h6>Materiaal hendel</h6>
                            <?php echo do_shortcode('[facetwp facet="product_filter"]'); ?>
                        </div>
                        <div class="col-9">
                            <div data-accessibility="0" data-adaptiveheight="0" data-autoplay="<?php echo $autoplay[0]; ?>" data-autoplayspeed="<?php echo $autoplayspeed; ?>" data-arrows="<?php echo $arrows[0]; ?>" data-centermode="<?php echo $centermode[0]; ?>"  data-centerpadding="0" data-dots="<?php echo $dots[0]; ?>" data-effect="scroll" data-swipe="1" data-easing="linear" data-infinite="<?php echo $infinite[0]; ?>" data-initialslide="1" data-pauseonhover="<?php echo $pauseonhover[0]; ?>" data-speed="300" data-variablewidth="0" data-slidestoscroll="<?php echo $slidestoscroll; ?>" data-slidestoshow="<?php echo $slidestoshow; ?>" data-responsive="1" data-slidestoscroll_1190="<?php echo $slidestoscroll1190; ?>" data-slidestoshow_1190="<?php echo $slidestoshow1190; ?>" data-slidestoscroll_768="<?php echo $slidestoscroll768; ?>" data-slidestoshow_768="<?php echo $slidestoshow768; ?>" data-slidestoscroll_480="<?php echo $slidestoscroll480; ?>" data-slidestoshow_480="<?php echo $slidestoshow480; ?>" class="<?php if($row[0] == 'row') { echo 'row '; } ?> <?php if(!empty($carousel)) { echo 'carousel'; } ?>">
                                <?php while ( $loop->have_posts() ) : $loop->the_post(); 
                                    get_template_part('/template-parts/loops/'.$folder_name.'/'.$folder_name);
                                endwhile; ?>
                            </div>   
                        </div>
                    </div>
                </div>
                <div class="pagination <?php $pagination_url.'-click' ?>">
                    <?php 
                        echo paginate_links( array(
                            'base'         => '?pagination&'.$pagination_url.'=%#%',
                            'total'        => $loop->max_num_pages,
                            'current'      => max( 1, $current_page),
                            'format'       => '?page=%#%',
                            'show_all'     => false,
                            'type'         => 'plain',
                            'end_size'     => 2,
                            'mid_size'     => 1,
                            'prev_next'    => true,
                            'prev_text'    => '<i class="rs rs-chevron-left"></i>',
                            'next_text'    => '<i class="rs rs-chevron-right"></i>',
                            'add_args'     => false,
                            'add_fragment' => '',
                        ) );
                    ?>
                </div>   
                <?php if(!empty($loader)) : ?>
                    <div class="loader-image">
                        <?php
                            $image_url = wp_get_attachment_image_src( $loader, 'medium');
                        ?>
                        <img src="<?php echo $image_url[0]; ?>">
                    </div> 
                <?php endif; ?>
            </div>  
            <?php if($ajaxify[0] == 'ajaxify') : ?>
                <script>
                    jQuery(function ($) {
                        $(document).on('click', '<?php echo '.container-'.$pagination_url ?> .page-numbers', function(e){
                            e.preventDefault();
                            $(this).closest('.loop-container').addClass('loading');
                            var href = $(this).attr('href');
                            var params = href.split('&');
                    
                            for (var i = 0; i < params.length; i++)
                            {
                                var sParameterName = params[i].split('=');

                                if (sParameterName[0] == 'term')
                                {
                                    var url_term = sParameterName[1];
                                }

                                if (sParameterName[0] == '<?php echo $pagination_url; ?>')
                                {
                                    var current = sParameterName[1];
                                }
                            }

                            if(url_term != "") {
                                var term_id = url_term;
                            } else {
                                var term_id = '<?php echo $term_id; ?>';
                            }
                            <?php 
                                if(!empty( $taxonomyfilter)) {
                                    $taxonomy =  $taxonomyfilter;
                                }
                            ?>

                            loadloopresults('<?php echo $post_type; ?>', '<?php echo $post_per_page; ?>', '<?php echo $taxonomy; ?>', term_id,  current, '<?php echo $pagination_url; ?>', '<?php echo $folder_name; ?>', '<?php echo $loop->max_num_pages; ?>' , '<?php if($row[0] == 'row') { echo 'row '; } ?>', '<?php echo $order_by; ?>', '<?php echo $order; ?>', '<?php echo $termfilter[0]; ?>', '<?php echo $taxonomyfilter; ?>', '<?php echo $loader; ?>', '<?php echo $carousel; ?>', '<?php echo $autoplay[0]; ?>', '<?php echo $autoplayspeed; ?>', '<?php echo $arrows[0]; ?>', '<?php echo $dots[0]; ?>', '<?php echo $pauseonhover[0]; ?>', '<?php echo $slidestoshow; ?>', '<?php echo $slidestoscroll; ?>', '<?php echo $slidestoshow1190; ?>', '<?php echo $slidestoscroll1190; ?>', '<?php echo $slidestoshow768; ?>', '<?php echo $slidestoscroll768; ?>', '<?php echo $slidestoshow480; ?>', '<?php echo $slidestoscroll480; ?>', '<?php echo serialize($selectspecificposts); ?>', '<?php echo $centermode[0]; ?>', <?php echo $infinite[0]; ?>);  
                            $('html, body').animate({
                                scrollTop: $(this).closest('.loop-container').offset().top - 200
                            }, 1000);      
                        });
                        
                        $(document).on('click', '.terms__block', function(e){
                            e.preventDefault();
                            $(this).closest('.loop-container').addClass('loading');
                            var term = $(this).data('term');
                            loadloopresults('<?php echo $post_type; ?>', '<?php echo $post_per_page; ?>', '<?php echo $taxonomyfilter; ?>', term,  0, '<?php echo $pagination_url; ?>', '<?php echo $folder_name; ?>', '<?php echo $loop->max_num_pages; ?>' , '<?php if($row[0] == 'row') { echo 'row '; } ?>', '<?php echo $order_by; ?>', '<?php echo $order; ?>', '<?php echo $termfilter[0]; ?>', '<?php echo $taxonomyfilter; ?>', '<?php echo $loader; ?>', '<?php echo $carousel; ?>', '<?php echo $autoplay[0]; ?>', '<?php echo $autoplayspeed; ?>', '<?php echo $arrows[0]; ?>', '<?php echo $dots[0]; ?>', '<?php echo $pauseonhover[0]; ?>', '<?php echo $slidestoshow; ?>', '<?php echo $slidestoscroll; ?>', '<?php echo $slidestoshow1190; ?>', '<?php echo $slidestoscroll1190; ?>', '<?php echo $slidestoshow768; ?>', '<?php echo $slidestoscroll768; ?>', '<?php echo $slidestoshow480; ?>', '<?php echo $slidestoscroll480; ?>', '<?php echo serialize($selectspecificposts); ?>', '<?php echo $centermode[0]; ?>', <?php echo $infinite[0]; ?>);
                        });
                    });
                </script>   
            <?php endif; ?>
        <?php endif;
    } else {
        if (!empty($_GET['post'])) {
            $post_type = $_GET['post'];
        } else {
            $post_type = get_the_id();
        }
        if(!empty($_GET[$pagination_url])) {
            $current_page = $_GET[$pagination_url];
        } else {
            $current_page = 1;
        }
        $args = array(  
            'post_type' => get_post_meta( $post_type, 'query', true ),
            'post_status' => 'publish',
            'posts_per_page' =>  $post_per_page, 
            'paged' =>  $current_page,
            'orderby' => $order_by, 
            'order' => $order,
        );

        $loop = new WP_Query( $args ); 

        if ($loop->have_posts()) : ?>
            <div class="loop-container <?php echo 'container-'.$pagination_url; ?>">
                <div data-accessibility="0" data-adaptiveheight="0" data-autoplay="<?php echo $autoplay[0]; ?>" data-autoplayspeed="<?php echo $autoplayspeed; ?>" data-arrows="<?php echo $arrows[0]; ?>" data-centermode="<?php echo $centermode[0]; ?>"  data-centerpadding="0" data-dots="<?php echo $dots[0]; ?>" data-effect="scroll" data-swipe="1" data-easing="linear" data-infinite="<?php echo $infinite[0]; ?>" data-initialslide="1" data-pauseonhover="<?php echo $pauseonhover[0]; ?>" data-speed="300" data-variablewidth="0" data-slidestoscroll="<?php echo $slidestoscroll; ?>" data-slidestoshow="<?php echo $slidestoshow; ?>" data-responsive="1" data-slidestoscroll_1190="<?php echo $slidestoscroll1190; ?>" data-slidestoshow_1190="<?php echo $slidestoshow1190; ?>" data-slidestoscroll_768="<?php echo $slidestoscroll768; ?>" data-slidestoshow_768="<?php echo $slidestoshow768; ?>" data-slidestoscroll_480="<?php echo $slidestoscroll480; ?>" data-slidestoshow_480="<?php echo $slidestoshow480; ?>" class="<?php if($row[0] == 'row') { echo 'row '; } ?> <?php if(!empty($carousel)) { echo 'carousel'; } ?>">
                    <?php while ( $loop->have_posts() ) : $loop->the_post(); 
                        get_template_part('/template-parts/loops/'.$folder_name.'/'.$folder_name);
                    endwhile; ?>
                </div>   
                <div class="pagination <?php $pagination_url.'-click' ?>">
                    <?php 
                        $term = '&term='.$term_id;
                        echo paginate_links( array(
                            'base'         => '?pagination=1&'.$pagination_url.'=%#%'.$term,
                            'total'        => $loop->max_num_pages,
                            'current'      => max( 1, $current_page),
                            'format'       => '?page=%#%',
                            'show_all'     => false,
                            'type'         => 'plain',
                            'end_size'     => 2,
                            'mid_size'     => 1,
                            'prev_next'    => true,
                            'prev_text'    => '<i class="rs rs-chevron-left"></i>',
                            'next_text'    => '<i class="rs rs-chevron-right"></i>',
                            'add_args'     => false,
                            'add_fragment' => '',
                        ) );
                    ?>
                </div>   
                <?php if(!empty($loader)) : ?>
                    <div class="loader-image">
                        <?php
                            $image_url = wp_get_attachment_image_src( $loader, 'medium');
                        ?>
                        <img src="<?php echo $image_url[0]; ?>">
                    </div> 
                <?php endif; ?>
            </div>  
            <?php if($ajaxify[0] == 'ajaxify') : ?>
                <script>
                    jQuery(function ($) {
                        $(document).on('click', '<?php echo '.container-'.$pagination_url ?> .page-numbers', function(e){
                            e.preventDefault();
                            $(this).closest('.loop-container').addClass('loading');
                            var href = $(this).attr('href');
                            var current = href.split('=')[1];
                            loadloopresults('<?php echo $post_type; ?>', '<?php echo $post_per_page; ?>', '<?php echo $taxonomy; ?>', '<?php echo $term_id; ?>',  current, '<?php echo $pagination_url; ?>', '<?php echo $folder_name; ?>', '<?php echo $loop->max_num_pages; ?>' , '<?php if($row[0] == 'row') { echo 'row '; } ?>', '<?php echo $order_by; ?>', '<?php echo $order; ?>', '<?php echo $loader; ?>', '<?php echo $carousel; ?>', '<?php echo $autoplay[0]; ?>', '<?php echo $autoplayspeed; ?>', '<?php echo $arrows[0]; ?>', '<?php echo $dots[0]; ?>', '<?php echo $pauseonhover[0]; ?>', '<?php echo $slidestoshow; ?>', '<?php echo $slidestoscroll; ?>', '<?php echo $slidestoshow1190; ?>', '<?php echo $slidestoscroll1190; ?>', '<?php echo $slidestoshow768; ?>', '<?php echo $slidestoscroll768; ?>', '<?php echo $slidestoshow480; ?>', '<?php echo $slidestoscroll480; ?>', '<?php echo $centermode[0]; ?>', <?php echo $infinite[0]; ?>);
                        });
                    });
                </script>    
            <?php endif ?>
        <?php endif;
    }
}

add_action('wp_ajax_nopriv_getpostsloop', 'getpostsloop');
add_action('wp_ajax_getpostsloop', 'getpostsloop');

function getpostsloop() {
    extract($_POST);
    $post_type = $_POST['postType'];
    $paged = $_POST['current'];
    $taxonomy = $_POST['taxonomy'];
    $term_id = $_POST['termID'];
    $postperpage = $_POST['postperpage'];
    $pagination_url = $_POST['paginationURL'];
    $template_part = $_POST['templatePart'];
    $posts_total = $_POST['postsTotal'];
    $row = $_POST['row'];
    $order_by = $_POST['orderby'];
    $order = $_POST['order'];
    $termfilter = $_POST['termfilter'];
    $taxonomyfilter = $_POST['taxonomyfilter'];
    $loader = $_POST['loader'];
    $carousel = $_POST['carousel'];
    $infinite = $_POST['infinite'];
    $autoplay = $_POST['autoplay'];
    $autoplayspeed = $_POST['autoplayspeed'];
    $arrows = $_POST['arrows'];
    $dots = $_POST['dots'];
    $pauseonhover = $_POST['pauseonhover'];
    $slidestoshow = $_POST['slidestoshow'];
    $slidestoscroll = $_POST['slidestoscroll'];
    $slidestoshow1190 = $_POST['slidestoshow1190'];
    $slidestoscroll1190 = $_POST['slidestoscroll1190'];
    $slidestoshow768 = $_POST['slidestoshow768'];
    $slidestoscroll768 = $_POST['slidestoscroll768'];
    $slidestoshow480 = $_POST['slidestoshow480'];
    $slidestoscroll480 = $_POST['slidestoscroll480'];
    $selectspecificposts = unserialize($_POST['selectspecificposts']);
    $centermode = $_POST['centermode'];


    $folder_name = basename($template_part);
    $args = array(  
        'post_type' => $post_type,
        'post_status' => 'publish',
        'posts_per_page' =>  $postperpage, 
        'paged' =>  $paged,
        'orderby' => $order_by, 
        'order' =>  $order,
        'post__in' => $selectspecificposts,
    );
    if(!empty($taxonomy) && $taxonomy != 'select' && !empty($term_id)) {
        $tax = array( 'tax_query' => array(
                array (
                    'taxonomy' => $taxonomy,
                    'field' => 'term_id',
                    'terms' => $term_id,
                    'exclude' => array('1')
                )
            ),
        );
        $args = array_merge($args, $tax);
    }
    $loop = new WP_Query( $args );
    ob_start();
    ?>
    <?php if($termfilter) : ?>
        <div class="terms">
            <a href="<?php echo home_url($_SERVER['REQUEST_URI']) ?>?term=" data-term="" class="terms__block<?php if (empty($term_id)) : ?>--active<?php endif; ?>">
                <?php _e('All Categories','mos')  ?>
            </a>
            <?php 
                $categories = get_terms($taxonomyfilter);
                foreach ($categories as $category) :
            ?>
                <a href="<?php echo home_url($_SERVER['REQUEST_URI']) ?>?term=<?php echo $category->term_id; ?>" data-term="<?php echo $category->term_id; ?>" class="terms__block<?php if ($term_id == $category->term_id) : ?>--active<?php endif; ?>">
                    <?php echo $category->name ?>
                </a>
            <?php
                endforeach;
            ?>
        </div>    
    <?php endif; ?>
    <div data-accessibility="0" data-adaptiveheight="0" data-autoplay="<?php echo $autoplay; ?>" data-autoplayspeed="<?php echo $autoplayspeed; ?>" data-centermode="<?php echo $centermode; ?>" data-arrows="<?php echo $arrows; ?>" data-centermode="0"  data-centerpadding="0" data-dots="<?php echo $dots; ?>" data-effect="scroll" data-swipe="1" data-easing="linear" data-infinite="<?php echo $infinite; ?>" data-initialslide="1" data-pauseonhover="<?php echo $pauseonhover; ?>" data-speed="300" data-variablewidth="0" data-slidestoscroll="<?php echo $slidestoscroll; ?>" data-slidestoshow="<?php echo $slidestoshow; ?>" data-responsive="1" data-slidestoscroll_1190="<?php echo $slidestoscroll1190; ?>" data-slidestoshow_1190="<?php echo $slidestoshow1190; ?>" data-slidestoscroll_768="<?php echo $slidestoscroll768; ?>" data-slidestoshow_768="<?php echo $slidestoshow768; ?>" data-slidestoscroll_480="<?php echo $slidestoscroll480; ?>" data-slidestoshow_480="<?php echo $slidestoshow480; ?>" class="<?php if(!empty($row)) { echo 'row '; } ?> <?php if(!empty($carousel)) { echo 'carousel'; } ?>">
        <?php
            while ( $loop->have_posts() ) : $loop->the_post(); 
                echo get_template_part('/template-parts/loops/'.$folder_name.'/'.$folder_name);
            endwhile; 
        ?>
    </div>
    <div class="pagination <?php echo $pagination_url.'-click' ?>">
        <?php 
            $term = '&term='.$term_id;
            echo paginate_links( array(
                'base'         => '?pagination=1&'.$pagination_url.'=%#%'.$term,
                'total'        => $loop->max_num_pages,
                'current'      => max( 1, $paged),
                'format'       => '?page=%#%',
                'show_all'     => false,
                'type'         => 'plain',
                'end_size'     => 2,
                'mid_size'     => 1,
                'prev_next'    => true,
                'prev_text'    => '<i class="rs rs-chevron-left"></i>',
                'next_text'    => '<i class="rs rs-chevron-right"></i>',
                'add_args'     => false,
                'add_fragment' => '',
            ) );
        ?>
    </div> 
    <?php if(!empty($loader)) : ?>
        <div class="loader-image">
            <?php
                $image_url = wp_get_attachment_image_src( $loader, 'medium');
            ?>
            <img src="<?php echo $image_url[0]; ?>">
        </div> 
    <?php endif; ?>
    <?php
    $res = ob_get_contents();
    ob_end_clean();
    echo json_encode($res);
    exit();
}

?>