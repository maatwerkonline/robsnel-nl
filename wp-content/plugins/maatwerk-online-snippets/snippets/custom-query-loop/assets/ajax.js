function loadloopresults(post_type, postperpage, taxonomy, term_id, current, pagination_url, template_part, posts_total, row, orderby, order, termfilter, taxonomyfilter, loader, carousel, autoplay, autoplayspeed, arrows, dots, pauseonhover, slidestoshow, slidestoscroll, slidestoshow1190, slidestoscroll1190, slidestoshow768, slidestoscroll768, slidestoshow480, slidestoscroll480, selectspecificposts, centermode, infinite) {
    jQuery(document).ready(function($){
        var data = {
            action: 'getpostsloop',
            postType: post_type,
            postperpage:  postperpage,
            taxonomy: taxonomy,
            termID: term_id,
            current: current,
            paginationURL: pagination_url,
            templatePart: template_part,
            postsTotal: posts_total,
            row: row,
            orderby: orderby,
            order: order,
            termfilter: termfilter,
            taxonomyfilter: taxonomyfilter,
            loader: loader,
            carousel: carousel,
            autoplay: autoplay,
            autoplayspeed: autoplayspeed,
            arrows: arrows,
            dots: dots,
            pauseonhover: pauseonhover,
            slidestoshow: slidestoshow,
            slidestoscroll: slidestoscroll,
            slidestoshow1190: slidestoshow1190,
            slidestoscroll1190: slidestoscroll1190,
            slidestoshow768: slidestoshow768,
            slidestoscroll768: slidestoscroll768,
            slidestoshow480: slidestoshow480,
            slidestoscroll480: slidestoscroll480,
            selectspecificposts: selectspecificposts,
            centermode: centermode,
            infinite: infinite,
        };

        
        // Ajax call
        $.ajax({
            type: "post",
            url: objectL10n.ajaxurl,
            data: data,
            dataType: 'json',
            cache: false,
            success: function (data) {
                $(".container-"+pagination_url).html(data);
                $('.loop-container').removeClass('loading');
                window.slickSlide();
            },
            'error': function (data) {
                alert('something went wrong');
            }
        });
    });
}
