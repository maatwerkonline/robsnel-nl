<?php

/**
 * Snippet Name: Form Functions
 * Description: Add additional Functions to Forms: Style Forms, Custom Gravityforms Loading Icon, Gravityforms Custom Button Classes.
 * Version: 1.0
 * Author: Nick Heurter
 * Required: true
 * Default Active: true
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


/**
 * Register Setting for Form Styles.
 */
function mo_form_styles_register_settings() {

    // Update all settings
    register_setting( 'form-styles-settings', 'form-style-setting' );
    register_setting( 'form-styles-settings', 'preview-form-setting' );

}
add_action( 'admin_init', 'mo_form_styles_register_settings' );


/**
 * Admin Menu for Form Styles.
 */
function mo_form_styles_admin_menu() {
    add_theme_page( 
        __('Form Styles', 'mos'),
        __('Form Styles', 'mos'),
        'manage_options',
        'form-styles',
        'mo_form_styles_settings',
    );
}
add_action( 'admin_menu', 'mo_form_styles_admin_menu' );


/**
 * Register Form Style Preview Post Type.
 */
function mo_register_form_style_preview_post_type() {
    $args = array(
        'label' => __( 'Form Style Preview' ),
        'description' => __( 'A post type created for previewing a choosen Style for Forms.' ),
        'public' => false,
        'publicly_queryable' => true,
        'has_archive' => true,
        'can_export' => false,
        'supports' => false,
        'rewrite' => array(
            'slug' => 'form-style-preview',
            'feeds' => false,
            'pages' => false
        )
    );

    register_post_type( 'form-style-preview', $args );

}
add_action( 'init', 'mo_register_form_style_preview_post_type' );

/**
 * Register Form Style Preview Post Type.
 */
function mo_maybe_load_preview_functionality( $wp_query ) {

    if( ! $wp_query->is_main_query() || ! is_post_type_archive( 'form-style-preview' ) ) {
        return;
    }

    mo_live_preview_hooks();

    $wp_query->query_vars['p'] = mo_get_preview_post( 'ID' );

    add_action( 'wp', 'mo_populate_post_content', 9 );

}
add_action( 'parse_query', 'mo_maybe_load_preview_functionality' );


/**
 * Populate Post Content for Gravityforms Scripts Styles.
 */
function mo_populate_post_content() {
    global $wp_query;

    foreach( $wp_query->posts as &$post ) {
        $post->post_content = mo_get_output_preview_form();
    }

}

/**
 * Get Preview Post.
 */
function mo_get_preview_post( $prop = false ) {

    $preview_posts = get_posts( array( 'post_type' => 'form-style-preview' ) );
    $preview_post = false;

    // If there are no preview posts, create one
    if( empty( $preview_posts ) ) {
        $post_id = wp_insert_post( array(
            'post_type' => 'form-style-preview',
            'post_title' => __( 'Form Style Preview', 'mos' ),
            'post_status' => 'publish'
        ) );
        $preview_post = get_post( $post_id );
    }
    // Otherwise, use the first preview post (there should only be one)
    else {
        $preview_post = $preview_posts[0];
    }

    if( ! $preview_post ) {
        return false;
    } else if( $prop ) {
        return $preview_post->$prop;
    } else {
        return $preview_post;
    }

}


/**
 * Live Preview Hooks.
 */
function mo_live_preview_hooks() {

    add_filter( 'template_include', 'mo_load_preview_template', 11 );
    add_filter( 'the_content', 'mo_modify_preview_post_content' );

}


/**
 * Load Preview Template.
 */
function mo_load_preview_template( $template ) {

    $page_template = get_page_template();
    if( $page_template )
        return $page_template;

    $single_template = get_single_template();
    if( $single_template )
        return $single_template;

    return $template;
}


/**
 * Modigy Preview Post Content.
 */
function mo_modify_preview_post_content( $content ) {
    return mo_get_output_preview_form();
}


/**
 * Get Shortcode Preview Form
 */
function mo_get_output_preview_form( $args = array() ) {

    if ( is_plugin_active( 'gravityforms/gravityforms.php' ) ) {

        if( ! is_user_logged_in() )
            return '<p>' . __( 'You need to log in to preview forms.' ) . '</p>' . wp_login_form( array( 'echo' => false ) );

        if( ! GFCommon::current_user_can_any( 'gravityforms_preview_forms' ) )
            return __( 'Oops! It doesn\'t look like you have the necessary permission to preview this form.' );

        if( empty( $args ) )
            $args = mo_get_preview_parameters_from_query_string();

        extract( wp_parse_args( $args ) );

        $title       = mo_is_true_value( $title ) ? 'true' : 'false';
        $description = mo_is_true_value( $description ) ? 'true' : 'false';
        $ajax        = mo_is_true_value( $ajax ) ? 'true' : 'false';

        return "[gravityform id='$id' title='$title' description='$description' ajax='$ajax']<style>header, footer, #wpadminbar {display: none !important;}</style>";

    } else {

        $output = '<div style="display:flex;justify-content: center;"><input id="search" class="text" name="search" type="text" maxlength="150" placeholder="'. __('Search...', 'mos') . '"></div>';
        $output .= '<style>header, footer, #wpadminbar {display: none !important;}</style>';

        return $output;
    }
}


/**
 * Get Shortcode Preview Form Parameters from Query String.
 */
function mo_get_preview_parameters_from_query_string() {
    return array_filter( array(
        'id'          => rgget( 'id' ),
        'title'       => rgget( 'title' ),
        'description' => rgget( 'description' ),
        'ajax'        => rgget( 'ajax' ),
        'style'       => rgget( 'style' )
    ) );
}


/**
 * Check if is true value
 */
function mo_is_true_value( $value ) {
    return $value === true || intval( $value ) === 1 || strtolower( $value ) === 'true';
}


/**
 * Add Help Tab.
 */
function mo_add_help_tab_forms_styles() {
	global $pagenow, $page;

	$screen = get_current_screen();

    // Content for 'Register Your Form Style' Tab
	ob_start(); ?>
    
        <h3><?php _e( 'Register Your Form Style', 'mos' ); ?></h3>
		<p><?php _e( 'If you would like to register your own Style for example from within your Theme or Plugin, you can use the following filter:', 'mos' ); ?></p>
        <code>
            function your_unique_function_name( $form_styles ) {<br>
            &nbsp;&nbsp;&nbsp;&nbsp;$form_styles['style-name'] = __('Style Name', 'your-textdomain');<br>
            &nbsp;&nbsp;&nbsp;&nbsp;return $form_styles;<br>
            }<br>
            add_filter( 'form_styles', 'your_unique_function_name');
        </code>

		<p><?php _e( 'Your Form Style will then be added to the dropdown below. After that, you only have to upload your Form Style CSS file to your theme folder like this:', 'mos' ); ?> <code>/assets/css/forms-style-{style-name}.css</code>. <?php _e('That\'s all!', 'mos' ); ?></p>
    
    <?php $content_register_your_style = ob_get_clean();

    // Add Help Tab 'Register Your Form Style'
	$screen->add_help_tab( array(
		'id'       => 'register_your_style',
		'title'    => __( 'Register Your Form Style', 'mos' ),
		'content'  => $content_register_your_style,
		'priority' => 10,
	) );

    // Content for 'Overwrite Form Style' Tab
    ob_start(); ?>

        <h3><?php _e( 'Overwrite Form Style', 'mos' ); ?></h3>
		<p><?php _e( 'If you would like to overwrite an existing Style, just copy the original file to your theme folder like this:', 'mos' ); ?> <code>/assets/css/forms-style-{style-name}.css</code>. <?php _e('That\'s all!', 'mos' ); ?></p>
    
    <?php $content_overwrite_style = ob_get_clean();

    // Add  Help Tab 'Overwrite Form Style'
    $screen->add_help_tab( array(
		'id'       => 'overwrite_style',
		'title'    => __( 'Overwrite Form Style', 'mos' ),
		'content'  => $content_overwrite_style,
		'priority' => 10,
	) );
}
add_action( 'admin_head-appearance_page_form-styles', 'mo_add_help_tab_forms_styles', 50 );


/**
 * Register Form Styles which will be Applied later in the Form Settings page.
 */
function mo_register_form_styles( $form_styles ) { 
    $form_styles['clean'] = __('Clean Design', 'mos');
    return $form_styles;
}
add_filter( 'form_styles', 'mo_register_form_styles');


/**
 * Settings for Form Styles.
 */
function mo_form_styles_settings() { 
    ?>

    <div class="wrap">

        <h1><?php _e('Form Styles', 'mos'); ?></h1>

        <div class="info-for-developers">
            <span><?php _e('Extra information for Developers', 'mos'); ?></span>
            <svg version="1.1" id="Laag_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 35 40" xml:space="preserve"><style type="text/css">.st0{fill:none;stroke:#000000;stroke-width:1.5;stroke-linecap:round;stroke-miterlimit:10;}.st1{fill:none;stroke:#000000;stroke-width:1.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}</style><g><path class="st0" d="M24.8,7.7c0.2,7.7,0.2,20.3-4.7,26.8c-5.4,7.3-11.4,0.6-16.9-3.8"/><path class="st1" d="M31.7,11.7c-3.8-2.3-4.5-6.6-7.6-9.5c-1.1,3-3.5,9.1-6.3,11.1"/></g></svg>
        </div>

        <?php 
        /* Apply_filters to receive all registered Form Styles */
        $form_styles = apply_filters( 'form_styles', $form_styles );
        ?>

        <form action="options.php" method="POST">

            <?php settings_fields( 'form-styles-settings' ); ?>
            <?php do_settings_sections( 'form-styles-settings' ); ?>
            <?php $option_form_style_setting = get_option( 'form-style-setting' ); ?>
            <?php $options_preview_form = get_option( 'preview-form-setting' ); ?>
   
            <div class="settings-preview-form">
                <div class="settings-preview-form-wrapper">
                
                    <label for="form_style"><?php _e('Form Style', 'mos'); ?></label>
                
                    <select name='form-style-setting' id="form_style">
                        <option value=""><?php _e('No style selected', 'mos'); ?></option>
                        <?php foreach ( $form_styles as $style_id => $style ) { ?>
                            <option value="<?php echo $style_id; ?>" <?php if( !empty( $option_form_style_setting ) ) { selected( $option_form_style_setting, $style_id ); }; ?>><?php echo $style; ?></option>
                        <?php } ?>
                    </select>

                    <?php if ( is_plugin_active( 'gravityforms/gravityforms.php' ) ) {
                        $forms = GFAPI::get_forms();
                        if( !empty( $forms ) ) { ?>
                            <label for="form_preview"><?php _e('Form to Preview', 'mos'); ?></label>

                            <select name='preview-form-setting' id="form_preview">
                                <?php 
                                foreach ( $forms as $form) { ?>
                                    <option value="<?php echo $form['id']; ?>" <?php if( !empty( $options_preview_form ) ) { selected( $options_preview_form, $style_id ); }; ?>><?php echo $form['title']; ?></option>
                                <?php }; ?>
                            </select>
                        <?php };
                    }; ?>

                    <button type="button" id="preview-form" class="button button-secondary"><?php _e('Preview', 'mos'); ?></button>

                    <a id="preview-form-full-screen" target="_blank"><svg fill="#2171b1" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" width="24px" height="24px"><path d="M 5 3 C 3.9069372 3 3 3.9069372 3 5 L 3 19 C 3 20.093063 3.9069372 21 5 21 L 19 21 C 20.093063 21 21 20.093063 21 19 L 21 12 L 19 12 L 19 19 L 5 19 L 5 5 L 12 5 L 12 3 L 5 3 z M 14 3 L 14 5 L 17.585938 5 L 8.2929688 14.292969 L 9.7070312 15.707031 L 19 6.4140625 L 19 10 L 21 10 L 21 3 L 14 3 z"/></svg></a>

                    <?php submit_button(__('Save Form Style', 'mos'), 'primary', '', false); ?>

                </div>
            </div>            

            <div class="preview-form-style">


                <div class="browser-mockup">
                    <iframe frameborder="0" width="1440" height="658" frameborder="0" allowfullscreen src="https://newdummy.devmaatwerkonline.nl/form-style-preview/?id=<?php echo $options_preview_form; ?>&title=false&description=false&ajax=true&style=<?php echo $option_form_style_setting; ?>"></iframe>
                </div>

            </div>

        </form>

        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('#preview-form').on('click', function() {
                    var form_style_setting = $('#form_style').val();
                    var preview_form_setting = $('#form_preview').val();
                    var src = '<?php echo get_site_url(); ?>/form-style-preview/?id=' + preview_form_setting + '&title=false&description=false&ajax=true&style=' + form_style_setting;

                    $('.browser-mockup iframe').attr('src', src);
                });
                $('#preview-form-full-screen').on('click', function() {
                    var form_style_setting = $('#form_style').val();
                    var preview_form_setting = $('#form_preview').val();
                    var src = '<?php echo get_site_url(); ?>/form-style-preview/?id=' + preview_form_setting + '&title=false&description=false&ajax=true&style=' + form_style_setting;

                    window.open(src, '_blank');
                    $('.browser-mockup iframe').attr('src', src);
                });
            });
        </script>

        <style>   
            .info-for-developers {
                position: absolute;
                right: 50px;
                margin-top: -10px;
            } 
            .info-for-developers span{
                width: 120px;
                display: inline-block;
                text-align: center;
                font-weight: 600;
                position: relative;
                top: -3px;
                left: 8px;
            } 
            .info-for-developers svg{
                width: 40px;
                height: 40px;
            } 
            .settings-preview-form {
                width: calc(80% - 20px);
                margin: 46px auto 23px;
                padding: 10px;
                overflow: hidden;
                background: #fbfbfb;
                border: 1px solid #e5e5e5;
                -webkit-box-shadow: 0 1px 1px rgb(0 0 0 / 4%);
                box-shadow: 0 1px 1px rgb(0 0 0 / 4%);
                display: flex;
                justify-content: center;
            }
            .settings-preview-form label {
                font-weight: 600;
                margin-right:10px;
            }

            #form_style, #form_preview {
                margin-right: 36px;
            }
            #preview-form-full-screen {
                cursor: pointer;
            }
            #preview-form-full-screen svg {
                width: 20px;
                height: 20px;
                position: relative;
                top: 5px;
                margin-right: 10px;
            }

            .browser-mockup {
                position: relative;
                width: 989.6px;
                margin: auto;
            }

            .browser-mockup:before {
                display: block;
                position: absolute;
                z-index: 9;
                content: '';
                top: 6px;
                left: 1em;
                width: 0.5em;
                height: 0.5em;
                border-radius: 50%;
                background-color: #f44;
                box-shadow: 0 0 0 2px #f44, 1.5em 0 0 2px #9b3, 3em 0 0 2px #fb5;
            }

            .browser-mockup iframe {
                display: block;
                width: 1440px;
                height: 658px;
                border-radius: 8px;
                -ms-zoom: 0.75;
                -moz-transform: scale(0.75);
                -moz-transform-origin: 0 0;
                -o-transform: scale(0.75);
                -o-transform-origin: 0 0;
                -webkit-transform: scale(68.722%);
                -webkit-transform-origin: 0 0;
                overflow: hidden;
                background-color: #fff;
                border-top: 2em solid rgb(29 35 39);
                box-shadow: rgb(149 157 165 / 20%) 0px 8px 24px;
                position: relative;
                background: url('/wp-admin/images/spinner-2x.gif') #fff no-repeat center center;
            }
        </style>

    </div>

    <?php
}


/**
 * Add Form Styles.
 * 
 * @link https://developer.wordpress.org/reference/functions/wp_enqueue_scripts/
 * @link https://developer.wordpress.org/reference/functions/wp_enqueue_style/
 */
function mo_forms_styles() {
    
    // On the archive page of the CPT 'Form Style Preview' load the Form Style based on the parameter in the url
    if( is_post_type_archive( 'form-style-preview' ) ) {

        if( empty( $args ) ) {
            $args = mo_get_preview_parameters_from_query_string();
        }
        $option_form_style_setting = $args['style'];

    // In all other situations, load the Form Style based on the setting 'form-style-setting'
    } else {
        $option_form_style_setting = get_option( 'form-style-setting' );
    };

    // If Gravityforms is active then adds dependency on 'gform_theme' and 'gform_basics' stylesheets
    // So that our Form Styles Stylesheet is loaded after these stylesheets
    if ( is_plugin_active( 'gravityforms/gravityforms.php' ) ) {
        $deps = array( 'gform_theme', 'gform_basic' );
    } else {
        $deps = '';
    };

    // Check if the Form Style exists in the Theme folder
    $filepath = get_template_directory() . '/assets/css/forms-style-'. $option_form_style_setting .'.css';
    if (file_exists($filepath)) {
        // If File exists in Theme folder, then load that file
        wp_enqueue_style( 'forms-style', get_stylesheet_directory_uri() . '/assets/css/forms-style-' . $option_form_style_setting . '.css', $deps );
    } else {
        // If File does NOT exists in Theme folder, then load the default file
        wp_enqueue_style( 'forms-style', plugin_dir_url( __DIR__ ) . 'form-functions/assets/css/forms-style-' . $option_form_style_setting . '.css', $deps );
    }

};
add_action('wp_enqueue_scripts', 'mo_forms_styles');


/**
 * Enqueue the Forms Styles for the Gutenberg block editor.
 */
function forms_styles_block_editor_assets() {
    $option_form_style_setting = get_option( 'form-style-setting' );
    // Check if the Form Style exists in the Theme folder
    $filepath = get_template_directory() . '/assets/css/forms-style-'. $option_form_style_setting .'.css';
    if (file_exists($filepath)) {
        // If File exists in Theme folder, then load that file
        add_editor_style( get_stylesheet_directory_uri() . '/assets/css/forms-style-' . $option_form_style_setting . '.css' );
    } else {
        // If File does NOT exists in Theme folder, then load the default file
        add_editor_style( plugin_dir_url( __DIR__ ) . 'form-functions/assets/css/forms-style-' . $option_form_style_setting . '.css' );
    }
    add_editor_style( TEMPPATH . '/assets/css/forms-style-one.css' );
}
add_action( 'admin_init', 'forms_styles_block_editor_assets', 100 );


/**
 * Functions Gravity Forms if Gravity Forms is active
 */
if ( is_plugin_active( 'gravityforms/gravityforms.php' ) ) {
    

    /**
     * Add custom Gravityforms Settings Fields.
     */
    function mo_custom_gform_form_settings_fields( $fields, $form ) {

        $fields['form_button']['fields'][] = array(
            'type'          => 'text',
            'name'          => 'text_after_submit_button',
            'label'         => __( 'Text after Submit Button', 'mos' ),
            'tooltip'       => __( 'Fill in a text which will be displayed after the submit button. HTML-allowed.', 'mos' ),
        );

        $fields['form_button']['fields'][] = array(
            'type'          => 'checkbox',
            'name'          => 'buttons_css_classes',
            'label'         => __( 'Buttons CSS-classes', 'mos' ),
            'tooltip'       => __( 'Fill in custom CSS-classes for the different buttons', 'mos' ),
            'choices' => array(
                array(
                    'label'         => esc_html__( 'Add custom CSS-classes to the buttons', 'sometextdomain' ),
                    'name'          => 'buttons_css_classes_checkbox',
                ),
            ),
        );
        
        $fields['form_button']['fields'][] = array(
            'type'          => 'text',
            'name'          => 'submit_button_css_class',
            'label'         => __( 'Submit Button Custom CSS-class', 'mos' ),
        );

        $fields['form_button']['fields'][] = array(
            'type'          => 'text',
            'name'          => 'previous_button_css_class',
            'label'         => __( 'Previous Button Custom CSS-class', 'mos' ),
        );

        $fields['form_button']['fields'][] = array(
            'type'          => 'text',
            'name'          => 'next_button_css_class',
            'label'         => __( 'Next Button Custom CSS-class', 'mos' ),
        );

        $fields['form_button']['fields'][] = array(
            'type'          => 'text',
            'name'          => 'save_continue_button_css_class',
            'label'         => __( 'Save and Continue Button Custom CSS-class', 'mos' ),
        );
    
        return $fields;
    }
    add_filter( 'gform_form_settings_fields', 'mo_custom_gform_form_settings_fields', 10, 2 );


    /**
     * Add Script to WordPress Backend for the checkbox 'buttons_css_classes_checkbox'.
     */
    function mo_add_gravityforms_settings_script_to_admin(){
        global $post;
        ?>
            <script type="text/javascript">
                jQuery(document).on("gform_load_form_settings", function(event, form){
                    if( jQuery('[name="_gform_setting_buttons_css_classes_checkbox"]').val() != 1 ) {
                        jQuery('#gform_setting_submit_button_css_class').hide();
                        jQuery('#gform_setting_previous_button_css_class').hide();
                        jQuery('#gform_setting_next_button_css_class').hide();
                        jQuery('#gform_setting_save_continue_button_css_class').hide();
                    };
                    jQuery('[name="_gform_setting_buttons_css_classes_checkbox"]').bind("change", function() {
                        jQuery('#gform_setting_submit_button_css_class').toggle();
                        jQuery('#gform_setting_previous_button_css_class').toggle();
                        jQuery('#gform_setting_next_button_css_class').toggle();
                        jQuery('#gform_setting_save_continue_button_css_class').toggle();
                    });
                });
            </script>
        <?php
    };
    add_action( 'admin_head', 'mo_add_gravityforms_settings_script_to_admin' );


    /**
     * Change Submit Button.
     */
    function mo_custom_gform_submit_buttons($button, $form){

        preg_match("/value='[\.a-zA-Z_ -]+'/", $button, $value);
        $value[0] = substr($value[0], 0, -1);
        $value[0] = str_replace( "value='", '', $value[0] );

        preg_match("/class='[\.a-zA-Z_ -]+'/", $button, $classes);
        $classes[0] = substr($classes[0], 0, -1);
        if( !empty( $form['submit_button_css_class'] ) && ( !empty( $form['buttons_css_classes_checkbox'] ) || $form['buttons_css_classes_checkbox'] == 1 ) ) {
            $classes[0] .= ' ' . esc_attr($form['submit_button_css_class']);
        };
        $classes[0] .= "'";

        $button_pieces = preg_split(
            "/class='[\.a-zA-Z_ -]+'/",
            $button,
            -1,
            PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY
        );

        $button_pieces[0] = str_replace( '<input', '<button', $button_pieces[0] );
        $button_pieces[1] = str_replace( "/", "", $button_pieces[1] );
        $button_pieces[2] = '</button>';
        if( !empty( $form['text_after_submit_button'] ) ) {
            $button_pieces[3] = $form['text_after_submit_button'];
        }

        return $button_pieces[0] . $classes[0] . $button_pieces[1] . $value[0] . $button_pieces[2] . $button_pieces[3];

    }
    add_filter( 'gform_submit_button', 'mo_custom_gform_submit_buttons', 10, 2 );


    /**
     * Change Next Button.
     */
    function mo_custom_gform_next_button($button, $form){

        preg_match("/value='[\.a-zA-Z_ -]+'/", $button, $value);
        $value[0] = substr($value[0], 0, -1);
        $value[0] = str_replace( "value='", '', $value[0] );

        preg_match("/class='[\.a-zA-Z_ -]+'/", $button, $classes);
        $classes[0] = substr($classes[0], 0, -1);
        if( !empty( $form['next_button_css_class'] ) && ( !empty( $form['buttons_css_classes_checkbox'] ) || $form['buttons_css_classes_checkbox'] == 1 ) ) {
            $classes[0] .= ' ' . esc_attr($form['next_button_css_class']);
        };
        $classes[0] .= "'";

        $button_pieces = preg_split(
            "/class='[\.a-zA-Z_ -]+'/",
            $button,
            -1,
            PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY
        );

        $button_pieces[0] = str_replace( '<input', '<button', $button_pieces[0] );
        $button_pieces[1] = str_replace( "/", "", $button_pieces[1] );
        $button_pieces[2] = '</button>';

        return $button_pieces[0] . $classes[0] . $button_pieces[1] . $value[0] . $button_pieces[2];

    }
    add_filter( 'gform_next_button', 'mo_custom_gform_next_button', 10, 2 );


    /**
     * Change Previous Button.
     */
    function mo_custom_gform_previous_button($button, $form){

        preg_match("/value='[\.a-zA-Z_ -]+'/", $button, $value);
        $value[0] = substr($value[0], 0, -1);
        $value[0] = str_replace( "value='", '', $value[0] );

        preg_match("/class='[\.a-zA-Z_ -]+'/", $button, $classes);
        $classes[0] = substr($classes[0], 0, -1);
        if( !empty( $form['previous_button_css_class'] ) && ( !empty( $form['buttons_css_classes_checkbox'] ) || $form['buttons_css_classes_checkbox'] == 1 ) ) {
            $classes[0] .= ' ' . esc_attr($form['previous_button_css_class']);
        };
        $classes[0] .= "'";

        $button_pieces = preg_split(
            "/class='[\.a-zA-Z_ -]+'/",
            $button,
            -1,
            PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY
        );

        $button_pieces[0] = str_replace( '<input', '<button', $button_pieces[0] );
        $button_pieces[1] = str_replace( "/", "", $button_pieces[1] );
        $button_pieces[2] = '</button>';

        return $button_pieces[0] . $classes[0] . $button_pieces[1] . $value[0] . $button_pieces[2];

    }
    add_filter( 'gform_previous_button', 'mo_custom_gform_previous_button', 10, 2 );


    /**
     * Change Save and Continue Link.
     */
    function mo_custom_gform_save_continue_link($button, $form){

        preg_match("/class='[\.a-zA-Z_ -]+'/", $button, $classes);
        $classes[0] = substr($classes[0], 0, -1);
        if( !empty( $form['save_continue_button_css_class'] ) && ( !empty( $form['buttons_css_classes_checkbox'] ) || $form['buttons_css_classes_checkbox'] == 1 ) ) {
            $classes[0] .= ' ' . esc_attr($form['save_continue_button_css_class']);
        };
        $classes[0] .= "'";

        $button_pieces = preg_split(
            "/class='[\.a-zA-Z_ -]+'/",
            $button,
            -1,
            PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY
        );

        $button_pieces[0] = str_replace( '<button', '<button', $button_pieces[0] );
        $button_pieces[1] = str_replace( "</button>", "</button>", $button_pieces[1] );

        if( !empty( $button_pieces[0] ) ){
            return $button_pieces[0] . $classes[0] . $button_pieces[1];
        };

    }
    add_filter( 'gform_savecontinue_link', 'mo_custom_gform_save_continue_link', 10, 2 );

    /**
     * Add Tooltip Setting to Standard Settings of all Gravityforms field types
     */
    function mo_add_tooltip_to_gform_field_standard_settings( $position, $form_id ) {
    
        // Create settings on position 20 (right after Field Label)
        if ( $position == 10 ) { ?>
            <li class="tooltip_setting field_setting">
                <label for="field_tooltip" class="section_label">
                    <?php _e('Label tooltip', 'mos'); ?>
                    <?php gform_tooltip("form_field_tooltip") ?>
                </label>
                <input type="text" id="field_tooltip" onchange="SetFieldProperty('tooltip', this.value);"/>
            </li>
            <?php
        }
    }
    add_action( 'gform_field_standard_settings', 'mo_add_tooltip_to_gform_field_standard_settings', 10, 2 );

    /**
     * Action to inject supporting script for the Tooltip Field to the form editor page
     */
    function mo_tooltip_editor_script(){
        ?>
        <script type='text/javascript'>
            // Adding setting to all field types
            jQuery.each(fieldSettings, function(index, value) {
                fieldSettings[index] += ", .tooltip_setting";
            });
            // Binding to the load field settings event to save the textarea
            jQuery(document).on('gform_load_field_settings', function(event, field, form){
                jQuery( '#field_tooltip' ).val( field['tooltip'] );
            });
        </script>
        <?php
    }
    add_action( 'gform_editor_js', 'mo_tooltip_editor_script' );

    /**
     * Add Tooltip to our Tooltip field
     */
    function mo_add_tooltip_tooltips( $tooltips ) {
        $tooltips['form_field_tooltip'] = '<h6>' . __('Label tooltip field', 'mos') . '</h6>' . __('Enter the Label tooltip for the Form Field. This will be dispayed to users and give them extra information about which information to fill in.', 'mos');
        return $tooltips;
    }
    add_filter( 'gform_tooltips', 'mo_add_tooltip_tooltips' );

    /**
     * Show Tooltip on frontend
     */
    function mo_add_tooltip_frontend($field_content, $field, $value, $lead_id, $form_id){

        if( $field['tooltip'] ) {
            $tooltip = '<span class="tooltip" data-tooltip-text="' . $field['tooltip'] . '" style="margin-left:10px;"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 122.88 122.88" width="15px" height="15px" xml:space="preserve"><g><path class="st0" d="M122.88,61.44C122.88,27.51,95.37,0,61.44,0C27.51,0,0,27.51,0,61.44c0,33.93,27.51,61.44,61.44,61.44 C95.37,122.88,122.88,95.37,122.88,61.44L122.88,61.44z M68.79,74.58H51.3v-1.75c0-2.97,0.32-5.39,1-7.25 c0.68-1.87,1.68-3.55,3.01-5.1c1.34-1.54,4.35-4.23,9.01-8.11c2.48-2.03,3.73-3.88,3.73-5.56c0-1.71-0.51-3.01-1.5-3.95 c-1-0.93-2.51-1.4-4.54-1.4c-2.19,0-3.98,0.73-5.4,2.16c-1.43,1.44-2.34,3.97-2.74,7.56l-17.88-2.22c0.61-6.57,3-11.86,7.15-15.85 c4.17-4.02,10.55-6.01,19.14-6.01c6.7,0,12.1,1.4,16.21,4.19c5.6,3.78,8.38,8.82,8.38,15.1c0,2.62-0.73,5.14-2.16,7.56 c-1.44,2.44-4.39,5.39-8.85,8.88c-3.09,2.48-5.05,4.44-5.86,5.93C69.19,70.24,68.79,72.19,68.79,74.58L68.79,74.58z M50.68,79.25 h18.76v16.53H50.68V79.25L50.68,79.25z"/></g></svg></span>';
        }
        $field_content = str_replace( '</label>',  $tooltip . '</label>', $field_content );

        return $field_content;

    };
    add_filter( 'gform_field_content', 'mo_add_tooltip_frontend', 10, 5);

};