var selectpickerIsClicked = false;
(function ($) {
    function update_preview(value, parent) {
        $('.icon_preview', parent).html('<i class="' + value + '" aria-hidden="true"></i>');
    }
    function select2_init(fa_field) {
        var $select = $(fa_field);
        var parent = $($select).closest('.acf-field');
        update_preview($select.val(), parent);
    }
    function select_custom_action() {
        var $fa_fields = $('select.customicons-edit:not(.select2_initalized)');
        if ($fa_fields.length) {
            $fa_fields.each(function (index, fa_field) {
                select2_init(fa_field);
            });
        }
        var $fa_field_edit = $('select.customicons-edit');
        if ($fa_field_edit.length) {
            $('select.customicons-edit > option').each(function () {
                if ($(this).text()) {
                    $(this).attr('data-content', $(this).text());
                    $(this).text('');
                }
            });
        }
        $('select.select2-customicons').selectpicker({
            container: 'body',
            dropupAuto: false
        });
        $('.dropdown-menu').on('click', function (e) {
            if ($(e.target).closest('.bootstrap-select.open').is(':visible') || $(e.target).closest('.btn.dropdown-toggle.btn-default').is(':visible')) {
                selectpickerIsClicked = true;
            }
        });

        $('.acf-button').on('click', function (e) {
            setTimeout(function () {
                $('select.select2-customicons').selectpicker({
                    container: 'body',
                    dropupAuto: false
                });
                $('.dropdown-toggle.disabled').remove();
        }, 200);
        });

        // when the dialog is closed....
        $('.dropdown').on('hide.bs.dropdown', function (e) {
            if (selectpickerIsClicked) {
                e.preventDefault();
                selectpickerIsClicked = false;
            }
        });
        // Update FontAwesome field previews when value changes
        $(document).on('change', 'select.select2-customicons', function () {
            var $input = $(this);
            if ($input.hasClass('customicons-edit')) {
                update_preview($input.val(), $input.closest('.acf-field'));
            }
        });
    }
    $(document).ready(function () {
//         var $cnt = 0;
//        setTimeout(function () {
//            select_custom_action();
//        }, 2000);
        var $cnt = 0;
        var $clerId = setInterval(function () {
            var $fa_field_edit = $('select.customicons-edit');
            if ($fa_field_edit.length) {
                $('select.customicons-edit > option').each(function () {
                    if ($(this).text()) {
                        $cnt++;
                    }
                });
            }
            if ($cnt) {
                select_custom_action();
                $cnt = 0;
                clearTimeout($clerId);
            }
        }, 500);
        $(document).on('click', '.components-button.components-toolbar__control', function (e) {
            var $clerId = setInterval(function () {
                var $fa_field_edit = $('select.customicons-edit');
                if ($fa_field_edit.length) {
                    $('select.customicons-edit > option').each(function () {
                        if ($(this).text()) {
                            $cnt++;
                        }
                    });
                }
                if ($cnt) {
                    select_custom_action();
                    $cnt = 0;
                    clearTimeout($clerId);
                }
            }, 1000);
        });
    });
})(jQuery);